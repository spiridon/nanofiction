/*
 *  NANOFICTION v1.0
 */

'use strict';

// Helper function
function includeFile(path, callback)
{
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.async = true;
  script.src = path;
  script.onload = function() {
    if(typeof(callback) == "function")
    {
      callback();
    }
  }
  try
  {
    var scriptOne = document.getElementsByTagName('script')[0];
    scriptOne.parentNode.insertBefore(script, scriptOne);
  }
  catch(e)
  {
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}

includeFile("helper-functions.js");
includeFile("game-initialization.js");

// Globals initialized in newgame()
var
  state, ui, milestones,
  map, things, player,
  words, aliases, refs;

/*
 *  UI UPDATE
 */

function updateNavbar() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | Health: " + Math.round(player.health.value) + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
}

function getReady() {
  updateNavbar();
  appendWords("GAME_START");
  waitForInput();
}

function loseGame() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | *** DEAD *** " + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
  appendWords("YOU_LOSE");
  appendLine("<i>Reload the page (F5) or type anything to play again</i>");
  state.gameState.value = "lost";
}

function winGame() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | *** FREE *** " + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
  appendWords("YOU_WIN!");
  appendLine("<i>Reload the page (F5) or type anything to play again</i>");
  state.gameState.value = "won";
}

// Append last command to game output
function appendInputValue(lines) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p class="inputvalue">>&nbsp;' + ui.input.value.toString() + '</p>';
}

// Append text to game output
function appendText(lines) {
  var a = document.getElementById("message");
  var l = "";
  for (let i = 0; i < lines.length; i++) {
    l = l + lines[i] + "<br/>";
  }
  ui.message.innerHTML =
    a.innerHTML + '<p>' + l + "</p>";
}

// Append a single line of text to game output
function appendLine(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p>' + line + '</p>';
}

// Manage paragraphs
function appendNewPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + '<p>' + line;
}

function appendToPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '<br/>';
}

function appendToPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '<br/>';
}

function appendEndPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '</p>';
}

// Append RAW text to game output
function appendRawText(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p>***' + line+ "***</p>";
}

function appendPlaceTitle(p) {
  var t = '<p class="placetitle">' + getTitle(p) + '</p>';
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + t;
}

/*
 *  WORDS PROCESSING
 */

function getTitle(s) {
  if (words.hasOwnProperty(s)) {
    if (words[s].hasOwnProperty("title")) {
      return words[s].title;
    }
  }
  return "***" + s + "***";
}

function getWord(w, s) {
  var r = "[" + s + ":" + w + "]";
  if (! words.hasOwnProperty(w)) { return r; }
  if (! words[w].hasOwnProperty(s)) { return r; }
  return words[w][s];
}

function processAliases(s) {
  var p = aliases[s];
  if (p == undefined) { return s; }
  else { return p; }
}

function processRefs(s) {
  Object.entries(refs).map(entry => {
    var key = entry[0];
    var v = entry[1];
    s = s.replace(key, v);
  });
  return s;
}

function capitalizeString(s) {
  return s.replace(/^\w/, (c) => c.toUpperCase());
}

function appendWords(s) {
  var p = words[s];
  if (p == undefined) {
    appendText(["[" + s + "]"]);
  } else {
    appendText(p);
  }
}

/*
 *  IN-GAME WORD PROCESSING
 */

function appendPlaceDescription(p) {
  if (words.hasOwnProperty(p) &&
    words[p].hasOwnProperty("description")
  ) {
    var a = words[p].description;
    appendText(a);
  } else {
    var s = "[[[DESCRIPTION: " + p + "]]]";
    appendText([s]);
  }
}

function appendObjectHere(o) {
  if (words.hasOwnProperty(o) &&
    words[o].hasOwnProperty("here")
  ) {
    appendText([ capitalizeString(words[o].here) ]);
  } else {
    var s = "[HERE: " + o + "]";
    appendText([s]);
  }
}

function appendObjectAccessible(o) {
  if (words.hasOwnProperty(o) &&
    words[o].hasOwnProperty("accessible")
  ) {
    appendText([ words[o].accessible ]);
  } else {
    var s = "[ACCESSIBLE: " + o + "]";
    appendText([s]);
  }
}

function appendInspection(o) {
  if (
    (words.hasOwnProperty(o)) &&
    (words[o].hasOwnProperty("inspection"))
  ) {
    appendText(words[o].inspection);
  } else {
    var s = "[INSPECTION: " + o + "]";
    appendText([s]);
  }
}

function appendAllNearby(p) {
  var f = 1;
  Object.entries(things).map(entry => {
    var key = entry[0];
    var l = things[key].locationInMap.value;
    if (l == p) {
      if (f > 0) {
        appendText(["You see:"]);
        f = 0;
      }
      appendObjectHere(key);
    } else if (
      things[key].hasOwnProperty("accessibleFrom") &&
      things[key].accessibleFrom == p
    ) {
      if (f > 0) {
        appendText(["You see:"]);
        f = 0;
      }
      appendObjectAccessible(key);
    }
  });
} 

function appendAllInside(p) {
  var f = 1;
  Object.entries(things).map(entry => {
    var key = entry[0];
    var l = things[key].locationInMap.value;
    if (l == p) {
      if (f > 0) {
//        appendNewPar(getWord(p, "hasInside"));
        appendLine(getWord(p, "hasInside"));
        f = 0;
      }
      if (things[key].hasOwnProperty("contains")) {
        if (things[key].amount.value == 0) {
//          appendToPar(getWord(key, "here") + " (empty)");
          appendLine(capitalizeString(getWord(key, "here")) + " (empty)");
        } else {
//          appendToPar(getWord(key, "here") + " (containing " + getWord(things[key].contains.value, "raw") + ")");
          appendLine(capitalizeString(getWord(key, "here")) + " (containing " + getWord(things[key].contains.value, "raw") + ")");
        }
      } else {
//        appendToPar(getWord(key, "here"));
        appendLine(capitalizeString(getWord(key, "here")));
      }
    }
  });
} 

/*
 *  IN-GAME FUNCTIONS
 */

function killObject(o) {
  var f = o[0];
  if (player.inventory.rightHand.value == f) {
    player.inventory.rightHand.value = undefined;
  } else if (player.inventory.leftHand.value == f) {
    player.inventory.leftHand.value = undefined;
  }
  delete things[f];
}

function isPitchBlack() {
  var h = player.locationInMap.value;
  if (map.pitchBlackPlaces.includes(h)) {
    if (
      (things.lantern.locationInMap.value == "player") &&
      (things.lantern.isOn.value == 1)
    ) { return false; }
    else if (
      (things.lantern.locationInMap.value == h) &&
      (things.lantern.isOn.value == 1)
    ) { return false; }
    else {
      if (things.lantern.isOn.value == 0) {
        return true;
      }
      var lh = things.lantern.locationInMap.value;
      if (things.hasOwnProperty(lh)) {
        if (things[lh].hasOwnProperty("isOpen")) {
          if (things[lh].isOpen.value == 1) {
            return false;
          }
        }
      }
    }
    return true;
  }
  return false;
}

function haveAvailableSlots() {
  if (player.inventory.rightHand.value == undefined ||
    player.inventory.leftHand.value == undefined
  ) {
    return true
  } else {
    return false
  }
}

function haveSword() {
  return (player.inventory.rightHand.value == "sword") ||
    (player.inventory.leftHand.value == "sword");
}

function takeChances(n, m) {
  return ( (m * Math.random()) > n );
}

function calculateDamage(i) {
  var h = player.health.value;
  if (h <= i) { return h; }
  var p = h - i;
  var d = 6.0 * Math.random();
  if (d >= p) { return h; }
  return d;
}

function runHook(h) {
  switch(h) {
    case "reach_restroom":
      things.dying_soldier.locationInMap.value = "limbo";
      things.soldier_corpse.locationInMap.value = "restroom";
      break;
    case "reach_vault":
      if (state.jackalAware.value == 0) {
        state.jackalAware.value = 1;
        if (isPitchBlack()) {
          if (haveSword()) {
            if (takeChances(player.health.value, 24)) {
              checkMilestone("killedJackalPitchBlack");
            } else {
              gameOver("deadFightingJackalPitchBlack");
            }
          } else {
            gameOver("slainedByJackalPitchBlack");
          }
        } else {
          if (haveSword()) {
            checkMilestone("killedJackal");
          } else {
            checkMilestone("jackalHasAwakened");
          }
        }
      } else {
        if (isPitchBlack()) {
          if (haveSword()) {
            if (takeChances(player.health.value, 32)) {
              checkMilestone("killedJackalPitchBlack");
            } else {
              gameOver("deadFightingJackalPitchBlack");
            }
          } else {
            gameOver("slainedByJackalPitchBlack");
          }
        } else {
          if (haveSword()) {
            checkMilestone("killedJackal");
          } else {
            gameOver("slainedByJackal");
          }
        }
      }
      break;
    case "jackalHasAwakened":
      if (! isPitchBlack()) {
        appendInspection("jackal");
      }
      appendLine("Horrified, you retreat to the cellar.");
      player.locationInMap.value = "cellar";
      milestones.reach_vault.done = 0;
      break;
    case "killedJackalPitchBlack":
      var d = calculateDamage(8);
      affectHealth(-d);
      things.jackal.locationInMap.value = "limbo";
      things.jackal_corpse.locationInMap.value = "vault";
      break;
    case "killedJackal":
      var d = calculateDamage(3);
      affectHealth(-d);
      things.jackal.locationInMap.value = "limbo";
      things.jackal_corpse.locationInMap.value = "vault";
      break;
    case "reach_exit":
      gameOver("exit");
      break;
  }
}

function checkMilestone(m) {
  if (milestones.hasOwnProperty(m)) {
    if (milestones[m].done == 0) {
      state.score.value = state.score.value + milestones[m].score;
      milestones[m].done = 1;
      runHook(m);
      if ((state.gameState.value == "lost") || (state.gameState.value == "won")) { return; }
      if (words.hasOwnProperty(m + "_ok")) {
        appendWords(m + "_ok");
//      } else {
//        appendLine("*** MILESTONE:" + m + " ***");
      }
    }
  }
}

function showScore(s) {
  appendText([
    "Score: " + state.score.value,
    s
  ]);
}

function showWinningScore() {
  var l = (state.timeLimit - state.elapsedTime.value);
  a.push("Bonus for remaining time: " + l);
  var h = Math.round(player.health.value);
  var bh = 0;
  if (h > 11) { a.push("Bonus for good health: " + h*2); bh = h*2; }
  var s = 0;
  for (var t in things) {
    if (things[t].locationInMap.value == "player") {
      s = s + things[t].score;
    }
  }
  if (things.bag.locationInMap.value == "player") {
    for (var t in things) {
      if (things[t].locationInMap.value == "bag") {
        s = s + things[t].score;
      }
    }
  }
  var c = 0;
  if (player.clothes.onBody.value = "soldier_uniform") {
    c = c + 20;
  }
  if (player.clothes.onFoot.value = "boots") {
    c = c + 10;
  }
  var w = 0;
  if (things.sword.locationInMap.value == "player") {
    w = w + 30;
  }
  var a = ["Game score: " + state.score.value];
  if (s > 0) { a.push("Bonus for items stolen: " + s); }
  if (c > 0) { a.push("Bonus for wearing soldier clothes: " + c); }
  if (w > 0) { a.push("Bonus for wielding a sword: " + w); }
  appendText(a);
  var f = state.score.value + l + bh + s + c + w;
  appendLine("<b>Your final score: " + f + "</b>")
}

function gameOver(s) {
  switch(s) {
    case "timeUp":
      appendWords("GAMEOVER:timeUp");
      showScore("You have been slained.");
      break;
    case "dead":
      appendWords("GAMEOVER:dead");
      showScore("You died of starvation.");
      break;
    case "deadFightingJackalPitchBlack":
      appendWords("GAMEOVER:deadFightingJackalPitchBlack");
      showScore("Fighting a hungry beast in the dark wasn't a good idea.");
      break;
    case "slainedByJackalPitchBlack":
      appendWords("GAMEOVER:slainedByJackalPitchBlack");
      showScore("You have been torn to pieces.");
      break;
    case "slainedByJackal":
      appendWords("GAMEOVER:slainedByJackal");
      showScore("The jackal ripped your body apart.");
      break;
    case "exit":
      appendWords("GAMEOVER:win");
      showWinningScore();
      winGame();
      return;
    default:
      appendWords("[[[GAMEOVER:" + s + "]]]");
      showScore(s);
  }
  loseGame();
}

function affectHealth(f) {
  var i = player.health.value;
  var d = i + f;
  player.health.value = d;
  if (d < 0.5) { gameOver("dead"); }
  else if (d < 3 && i < d) { appendWords("feelBetterButStarving"); }
  else if (d < 3) { appendWords("feelStarving"); }
  else if (d < 5 && i < d) { appendWords("feelBetterButIll"); }
  else if (d < 5) { appendWords("feelIll"); }
  else if (d > 4 && i < d) { appendWords("feelBetter"); }
  else if (d < 8  && i > d) { appendWords("feelNotTooGood"); }
  else if (d > 7  && i < d) { appendWords("feelAwesome"); }
}

function checkLantern() {
  if (
    things.lantern.isOn.value == 1 &&
    things.lantern.timeOut.value > 0
  ) {
    things.lantern.timeOut.value = things.lantern.timeOut.value - 1;
    if (things.lantern.locationInMap.value == "player") {
      if (things.lantern.timeOut.value < 1) {
        appendWords("LANTERN_IS_DEAD");
        things.lantern.isOn.value = 0;
        if (isPitchBlack()) { appendWords("PITCH_BLACK"); }
      }
      if (
        things.lantern.timeOut.value > 0 &&
        things.lantern.timeOut.value < 8
      ) {
        appendWords("LANTERN_IS_ABOUT_TO_DIE");
      }
    }
  }
}

function advanceTime(i) {
  if (
    (state.gameState.value != "lost") &&
    (state.gameState.value != "won")
  ) {
    state.elapsedTime.value += i;
    affectHealth(-i*0.28);
    checkLantern();
    if (state.elapsedTime.value >= state.timeLimit) {
      gameOver("timeUp");
    }
  }
}

/*
 *  COMMANDS
 */

//==========================================================
// LOOK
function doLook(a) {
  if (a[0] == "at") {
    a = stripFirst(a);
    if (a[0] == undefined) {
      appendWords("BAD_SYNTAX");
      return;
    }
    else { doLookAt(a); }
  } else if (a[0] == undefined || a[0] == "around") {
    doLookAround();
  } else {
    doLookAt(a);
  }
  advanceTime(1);
}

function doLookAround() {
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK");
  } else {
    var h = player.locationInMap.value;
    appendPlaceTitle(h);
    appendPlaceDescription(h);
    appendAllNearby(h);
  }
}

function doLookAt(a) {
  doInspect(a);
}

//==========================================================
// INSPECT
function doInspect(o) {
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK");
    return;
  }
  if (things.hasOwnProperty(o)) {
    tryInspectThing(o);
  } else if (map.doors.hasOwnProperty(o)) {
    tryInspectDoor(o);
  } else {
    appendWords("NOT_FOUND");
  }
}

function tryInspectThing(o) {
  var h = player.locationInMap.value;
  if (things[o].locationInMap.value == h) {
    if (things[o].hasOwnProperty("contains")) {
      if (things[o].amount.value == 0) {
        appendLine(capitalizeString(getWord(o, "here")) + " (empty)");
      } else {
        appendLine(capitalizeString(getWord(o, "here")) + " (containing " + getWord(things[o].contains.value, "raw") + ")");
      }
    } else {
      appendInspection(o);
      if (
        (! things[o].hasOwnProperty("isOpen")) ||
        (things[o].isOpen.value == 1)
      ) {
        appendAllInside(o);
      }
    }
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    if (things[o].hasOwnProperty("contains")) {
      if (things[o].amount.value == 0) {
        appendLine(capitalizeString(getWord(o, "here")) + " (empty)");
      } else {
        appendLine(capitalizeString(getWord(o, "here")) + " (containing " + getWord(things[o].contains.value, "raw") + ")");
      }
    } else {
      appendInspection(o);
      if (
        (! things[o].hasOwnProperty("isOpen")) ||
        (things[o].isOpen.value == 1)
      ) {
        appendAllInside(o);
      }
    }
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          appendInspection(o);
          appendAllInside(o);
        } else {
          appendWords("NOT_FOUND"); return;
        }
      } else {
        appendWords("NOT_FOUND"); return;
      }
    } else {
      appendWords("NOT_FOUND"); return;
    }
  }
  advanceTime(1);
}

function tryInspectDoor(o) {
  var h = player.locationInMap.value;
  // Check if the door is here
  if (map.doors[o].connects.hasOwnProperty(h)) {
    appendInspection(o);
    var open, locked;
    if (map.doors[o].isOpen.value == 1) { open = "open"; }
    else { open = "closed"; }
    if (map.doors[o].hasOwnProperty("isLocked")) {
      if (map.doors[o].isLocked.value == 1) { locked = "locked"; }
      else { locked = "unlocked"; }
    } else { locked = ""; }
    if (open == "open") {
      appendLine(capitalizeString(getWord(o, "denotative") + " is open."));
    } else if (open == "closed" && locked == "unlocked") {
      appendLine(capitalizeString(getWord(o, "denotative") + " is closed."));
    } else {
      appendLine(capitalizeString(getWord(o, "denotative") + " is locked."));
    }
  } else {
    appendWords("NOT_FOUND"); return;
  }
  advanceTime(1);
}

//==========================================================
// PICK
function performPick(o) {
  var s = undefined;
  if (player.inventory.rightHand.value == undefined) {
    s = "rightHand";
  } else {
    s = "leftHand";
  }
  things[o].locationInMap.value = "player";
  player.inventory[s].value = o;
}

function tryPick(o) {
  if (things[o].hasOwnProperty("canBePicked")) {
    if (things[o].canBePicked == 1) {
      if (haveAvailableSlots()) {
        performPick(o);
        if (words.hasOwnProperty(o) &&
          words[o].hasOwnProperty("raw")
        ) {
          appendText([capitalizeString(words[o].raw) + ": picked."]);
        } else {
          appendText(["[RAW:"+o+"]" + ": picked."]);
        }
        advanceTime(1);
      } else {
        appendWords("NO_AVAILABLE_SLOTS");
      }
    } else {
      appendWords("CANNOT_PICK_THAT");
    }
  } else {
    appendWords("CANNOT_PICK_THAT");
  }
}

function doPick(o) {
  var h = player.locationInMap.value;
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK"); return;
  }
  if (! things.hasOwnProperty(o)) {
    appendWords("NOT_FOUND");
  } else if (things[o].locationInMap.value == h) {
    tryPick(o);
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    tryPick(o);
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          tryPick(o);
        } else {
          appendWords("NOT_FOUND");
        }
      } else {
        appendWords("NOT_FOUND");
      }
    } else {
      appendWords("NOT_FOUND");
    }
  }
}

//==========================================================
// WEAR
function performWear(o, s) {
  things[o].locationInMap.value = "player";
  player.clothes[s].value = o;
}

function tryWear(o) {
  if (things[o].hasOwnProperty("wearAs")) {
    if (things[o].canBePicked == 1) { // Redundant...
      var slot = things[o].wearAs;
      if (player.clothes[slot].value == undefined) {
        performWear(o, slot);
        appendText(["Now wearing: " + getWord(o, "raw")]);
        advanceTime(1);
      } else {
        appendWords("NO_AVAILABLE_SLOTS_FOR_CLOTHES");
      }
    } else {
      appendWords("CANNOT_PICK_THAT");
    }
  } else {
    appendWords("CANNOT_WEAR_THAT");
  }
}

function doWear(o) {
  var h = player.locationInMap.value;
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK"); return;
  }
  if (! things.hasOwnProperty(o)) {
    appendWords("NOT_FOUND");
  } else if (things[o].locationInMap.value == h) {
    tryWear(o);
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    tryWear(o);
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          tryWear(o);
        } else {
          appendWords("NOT_FOUND");
        }
      } else {
        appendWords("NOT_FOUND");
      }
    } else {
      appendWords("NOT_FOUND");
    }
  }
}

//==========================================================
// DROP
function doDrop(o) {
  var h = player.locationInMap.value;
  var k = o[0];
  var s = capitalizeString(getWord(o, "raw")) + ": dropped.";
  if (player.inventory.rightHand.value == k) {
    things[o].locationInMap.value = h;
    player.inventory.rightHand.value = undefined;
    appendText([s]);
  } else if (player.inventory.leftHand.value == k) {
    things[o].locationInMap.value = h;
    player.inventory.leftHand.value = undefined;
    appendText([s]);
  } else if (player.clothes.onBody.value == k) {
    things[o].locationInMap.value = h;
    player.clothes.onBody.value = undefined;
    appendText([s]);
  } else if (player.clothes.onFoot.value == k) {
    things[o].locationInMap.value = h;
    player.clothes.onFoot.value = undefined;
    appendText([s]);
  } else {
    appendWords("CANNOT_DROP"); return;
  }
  advanceTime(1);
}

//==========================================================
// PUT
function doPut(a) {
  var t = a[0];
  var w = a[1];
  var c = a[2];
  var v = ["in", "on", "over"]
  var h = player.locationInMap.value;
  if (t == undefined) { appendWords("PUT_WHAT"); return; }
  if ( w == undefined || (! v.includes(w) )) {
    appendWords("BAD_SYNTAX"); return;
  }
  if (c == undefined) { appendWords("BAD_SYNTAX"); return; }
  if (! things.hasOwnProperty(t)) {
    appendWords("NOT_FOUND"); return;
  }
  if (things[t].locationInMap.value != "player") {
    appendWords("NOT_IN_INVENTORY"); return;
  }
  if (! things.hasOwnProperty(c)) {
    appendWords("NOT_FOUND"); return;
  }
  if (
    (things[c].locationInMap.value != "player") &&
    (things[c].locationInMap.value != h)
  ) {
    appendWords("CONTAINER_NOT_HERE"); return;
  }
  if (! things[c].hasOwnProperty("isContainer")) {
    appendWords("NOT_A_CONTAINER"); return;
  }
  if (things[c].containerType != w) {
    appendWords("PHRASED_INCORRECTLY"); return;
  }
  // All OK, proceeding...
  if (player.inventory.rightHand.value == t) {
    player.inventory.rightHand.value = undefined;
  } else if (player.inventory.leftHand.value == t) {
    player.inventory.leftHand.value = undefined;
  } else if (player.clothes.onBody.value == t) {
    player.clothes.onBody.value = undefined;
  } else if (player.clothes.onFoot.value == t) {
    player.clothes.onFoot.value = undefined;
  }
  things[t].locationInMap.value = c;
  appendWords("DONE");
  advanceTime(1);
}

//==========================================================
// OPEN
function doOpen(d) {
  if (d[0] == undefined) {
    appendWords("OPEN_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];

  // The chest opens differently
  if (c == "chest") {
    var h = player.locationInMap.value;
    if (h == "vault") {
      if (things.chest.isOpen == 1) {
        appendWords("ALREADY_OPEN"); return;
      }
      if (
        (things.nail_bar.locationInMap.value == "player") &&
        (things.hammer.locationInMap.value == "player")
      ) {
        things.chest.isOpen.value = 1;
        things.chest.isLocked.value = 0;
        appendWords("OPENING_CHEST");
        checkMilestone("open_" + c);
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_TRY_BUT_FAIL"); return;
      }
    } else {
      appendWords("NOT_FOUND"); return;
    }
  }

  // Everything but the chest
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isOpen.value == 1) {
      appendWords("ALREADY_OPEN"); return;
    }
    if (
      map.doors[c].hasOwnProperty("isLocked") &&
      map.doors[c].isLocked.value == 1
    ) {
      appendWords("IT_IS_LOCKED"); return;
    }
    map.doors[c].isOpen.value = 1;
    appendWords("OPENING_IT");
    checkMilestone("open_" + c);
    advanceTime(1);
    return;
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isOpen")) {
      if (things[c].isOpen.value == 1) {
        appendWords("ALREADY_OPEN"); return;
      }
      if (
        things[c].hasOwnProperty("isLocked") &&
        things[c].isLocked.value == 1
      ) {
        appendWords("IT_IS_LOCKED"); return;
      }
      things[c].isOpen.value = 1;
      appendWords("OPENING_IT");
      checkMilestone("open_" + c);
      advanceTime(1);
      return;
    } else {
      appendWords("CANNOT_BE_OPENED"); return;
    }
  } else {
    appendWords("OPEN_WHAT"); return;
  }
}

//==========================================================
// CLOSE
function doClose(d) {
  if (d[0] == undefined) {
    appendWords("CLOSE_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isOpen.value == 0) {
      appendWords("ALREADY_CLOSE"); return;
    }
    map.doors[c].isOpen.value = 0;
    appendWords("CLOSING_IT");
    advanceTime(1);
    return;
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isOpen")) {
      if (things[c].isOpen.value == 0) {
        appendWords("ALREADY_CLOSED"); return;
      }
      things[c].isOpen.value = 0;
      appendWords("CLOSING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("CANNOT_BE_CLOSED"); return;
    }
  } else {
    appendWords("CLOSE_WHAT"); return;
  }
}

//==========================================================
//  LOCK
function doLock(d) {
  if (d[0] == undefined) {
    appendWords("LOCK_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isLocked.value == 1) {
      appendWords("ALREADY_LOCKED"); return;
    }
    var l = map.doors[c].hasLock;
    if (
      (things.hasOwnProperty(l)) &&
      (things[l].locationInMap.value == "player")
    ) {
      map.doors[c].isLocked.value = 1;
      appendWords("LOCKING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("NEED_THE_KEY"); return;
    }
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isLocked")) {
      if (things[c].isLocked.value == 1) {
        appendWords("ALREADY_LOCKED"); return;
      }
      var l = things[c].hasLock;
      if (
        (things.hasOwnProperty(l)) &&
        (things[l].locationInMap.value == "player")
      ) {
        things[c].isLocked.value = 1;
        appendWords("LOCKING_IT");
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_DONT_HAVE_THE_KEY"); return;
      }
    } else {
      appendWords("CANNOT_BE_LOCKED"); return;
    }
  } else {
    appendWords("LOCK_WHAT"); return;
  }
}

//==========================================================
// UNLOCK
function doUnlock(d) {
  if (d[0] == undefined) {
    appendWords("UNLOCK_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isLocked.value == 0) {
      appendWords("ALREADY_UNLOCKED"); return;
    }
    var l = map.doors[c].hasLock;
    if (
      (things.hasOwnProperty(l)) &&
      (things[l].locationInMap.value == "player")
    ) {
      map.doors[c].isLocked.value = 0;
      appendWords("UNLOCKING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("NEED_THE_KEY"); return;
    }
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isLocked")) {
      if (things[c].isLocked.value == 0) {
        appendWords("ALREADY_UNLOCKED"); return;
      }
      var l = things[c].hasLock;
      if (
        (things.hasOwnProperty(l)) &&
        (things[l].locationInMap.value == "player")
      ) {
        things[c].isLocked.value = 0;
        appendWords("UNLOCKING_IT");
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_DONT_HAVE_THE_KEY"); return;
      }
    } else {
      appendWords("CANNOT_BE_UNLOCKED"); return;
    }
  } else {
    appendWords("UNLOCK_WHAT"); return;
  }
}

//==========================================================
// DISPLACE 
function doDisplace(a) {
  if (a[0] == undefined) {
    appendWords("GO_WHERE");
    return;
  }
  var here = player.locationInMap.value;
  var dir = a[0];
  var dest = map.pathways[here][dir];
  if (dest == undefined) {
    // Check if the direction is valid
    appendWords("CANNOT_GO_THERE");
  } else {
    // Check if there is a door
    for (var door in map.doors) {
      if (
        map.doors[door]["connects"].hasOwnProperty(here)
      &&
        map.doors[door]["connects"][here] == dir
      &&
        map.doors[door]["isOpen"]["value"] == 0
      ) {
        var s = getWord(door, 'denotative');
        s = capitalizeString(s);
        appendText([s + " is closed."]);
        return;
      }
    }
    //player["locationInMap"] = dest;
    player["locationInMap"]["value"] = dest;
    doLookAround();
    advanceTime(1);
    checkMilestone("reach_" + dest);
  }
}

//==========================================================
// EAT
function doEat(f) {
  if (f == undefined) {
    appendWords("DRINK_WHAT"); return;
  }
  if (! things.hasOwnProperty(f)) {
    appendWords("NOT_FOUND"); return;
  }
  if (things[f].locationInMap.value != "player") {
    appendWords("NOT_IN_INVENTORY"); return;
  }
  if (! things[f].hasOwnProperty("isEdible")) {
    appendWords("NOT_EDIBLE"); return;
  }
  // Eating it
  appendWords("YUMMY");
  affectHealth(things[f].nutritionalValue);
  things[f].amount.value = things[f].amount.value - 1;
  if (things[f].amount.value == 0) {
    killObject(f);
    appendText([getWord(f, "consumed")]);
  }
  advanceTime(1);
}

//==========================================================
// DRINK
function doDrink(a) {
  var l = a[0];
  if (l == undefined) {
    appendWords("DRINK_WHAT"); return;
  }
  var c;
  var u;
  c = player.inventory.rightHand.value;
  if (
    (c != undefined) &&
    (things[c].hasOwnProperty("contains")) &&
    (things[c].contains.value == l) &&
    (things[c].amount.value > 0)
  ) { u = c; }
  if (u == undefined) {
    c = player.inventory.leftHand.value;
    if (
      (c != undefined) &&
      (things[c].hasOwnProperty("contains")) &&
      (things[c].contains.value == l) &&
      (things[c].amount.value > 0)
    ) { u = c; }
  }
  if (u == undefined) {
    appendWords("NOT_FOUND"); return;
  }
  // Drinking it
  appendWords("SLURP");
  affectHealth(2);
  things[u].amount.value = things[u].amount.value - 1;
  if (things[u].amount.value == 0) {
    appendText([getWord(u, "empty")]);
  }
  advanceTime(1);
}

//==========================================================
// INVENTORY
function doInventory() {
  var f = 0;
  if (player.inventory.rightHand.value != undefined) {
    if (f == 0) {
      f = 1;
      appendText(["You carry:"]);
    }
    appendText([capitalizeString(getWord(player.inventory.rightHand.value, "here")) + " (right hand)"]);
  }
  if (player.inventory.leftHand.value != undefined) {
    if (f == 0) {
      f = 1;
      appendText(["You carry:"]);
    }
    appendText([capitalizeString(getWord(player.inventory.leftHand.value, "here")) + " (left hand)"]);
  }
  var w = 0;
  if (player.clothes.onBody.value != undefined) {
    if (w == 0) {
      w = 1;
      appendText(["You wear:"]);
    }
    appendText([capitalizeString(getWord(player.clothes.onBody.value, "here")) + " (on body)"]);
  }
  if (player.clothes.onFoot.value != undefined) {
    if (w == 0) {
      w = 1;
      appendText(["You wear:"]);
    }
    appendText([capitalizeString(getWord(player.clothes.onFoot.value, "here")) + " (on foot)"]);
  }
  if (w == 0) {
    appendText(["You are naked."]);
  }
}

//==========================================================
// LANTERN
function doLantern(a) {
  var s = a[0];
  if (things.lantern.locationInMap.value != "player") {
    appendWords("NOT_FOUND"); return;
  }
  var dark = isPitchBlack();
  switch(s) {
    case "on":
      if (things.lantern.timeOut < 1) {
        appendWords("LANTERN_ALREADY_DEAD");
      } else if (things.lantern.isOn.value == 1) {
        appendWords("LANTERN_ALREADY_ON");
      } else {
        things.lantern.isOn.value = 1;
        appendWords("LANTERN_IS_NOW_ON");
        if (dark) { doLookAround(); }
        else { advanceTime(1) };
      }
      break;
    case "off":
      if (things.lantern.timeOut < 1) {
        appendWords("LANTERN_ALREADY_DEAD");
      } else if (things.lantern.isOn.value == 0) {
        appendWords("LANTERN_ALREADY_OFF");
      } else {
        things.lantern.isOn.value = 0;
        appendWords("LANTERN_IS_NOW_OFF");
        if (isPitchBlack()) { doLookAround(); }
      }
      break;
  }
}

//==========================================================
// HELP
function doHelp(a) {
  appendWords("HELP");
}

/*
 *  GAME ENGINE
 */

// Compute guess
function handleInput() {
  if ((state.gameState.value == "lost") || (state.gameState.value == "won")) {
    window.location.reload(true);
  }
  // Show command in game output
  appendInputValue();

  // Get the user's guess from the input field
  var gp = this.value.toString().toLowerCase();
  var gpp = processRefs(gp);
  var g = processAliases(gpp);
  var a = g.split(" ");

  switch(a[0]) {
    case "look":
      doLook(stripFirst(a));
      break;
    case "inspect":
      doInspect(stripFirst(a));
      break;
    case "go":
      doDisplace(stripFirst(a));
      break;
    case "pick":
      doPick(stripFirst(a));
      break;
    case "drop":
      doDrop(stripFirst(a));
      break;
    case "put":
      doPut(stripFirst(a));
      break;
    case "wear":
      doWear(stripFirst(a));
      break;
    case "open":
      doOpen(stripFirst(a));
      break;
    case "close":
      doClose(stripFirst(a));
      break;
    case "lock":
      doLock(stripFirst(a));
      break;
    case "unlock":
      doUnlock(stripFirst(a));
      break;
    case "eat":
      doEat(stripFirst(a));
      break;
    case "drink":
      doDrink(stripFirst(a));
      break;
    case "help": //
      doHelp();
      break;
    case "inventory":
      doInventory(stripFirst(a));
      break;
    case "lantern":
      doLantern(stripFirst(a));
      break;
    default:
      appendWords("COMMAND_NOT_FOUND");
      break;
  }
  
  updateNavbar();
  waitForInput();
}

