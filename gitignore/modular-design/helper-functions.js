/*
 *  HELPER FUNCTIONS
 */

// Scroll right to the bottom of the document
function scrollDown() {
  // Scroll down
  var scrollHeight = document.body.scrollHeight;
  window.scrollTo(0, scrollHeight);
}

function waitForInput() {
  ui.input.style.visibility = "visible";
  ui.input.value = "";
  ui.input.focus();
  scrollDown();
}

function stripFirst(a) {
  //return a.reverse().pop().reverse();
  var t = a.reverse();
  t.pop();
  return t.reverse();
}

