/*
 *  GAME INITIALIZATION
 */

// Initialize
function newgame() {
  // Set up an object to hold document elements we care about
  ui = {
    scoreboard: null,
    message: null,  // The game response.
    input: null,     // Where the user enters the commands.
    userinput: null
  };
  
  // Initialize each of these element ids
  for(var id in ui) ui[id] = document.getElementById(id);
    
  // Define an event handler for the input field
  ui.input.onchange = handleInput;

  //=========================================================
  // Milestones
  milestones = {
    open_gate: { score: 5, done: 0 },
    reach_aisle: { score: 0, done: 0 },
    reach_restroom: { score: 10, done: 0 },
    reach_cellar: { score: 0, done: 0 },
    reach_vault: { score: 15, done: 0 },
    killedJackal: { score: 60, done: 0 },
    killedJackalPitchBlack: { score: 75, done: 0 },
    jackalHasAwakened: { score: 7, done: 0 },
    open_chest: { score: 20, done: 0 },
    open_safe: { score: 30, done: 0 },
    reach_exit: { score: 100, done: 0 }
  };

  //=========================================================
  // World
  map = {
    pathways: {
      study: { east: "hall" },
      hall: { west: "study", east: "lobby", south: "corridor" },
      lobby: { west: "hall", east: "exit" },
      corridor: { north: "hall", south: "aisle", down: "cellar" },
      restroom: { east: "aisle" },
      aisle: { west: "restroom", east: "cell", north: "corridor" },
      cell: { west: "aisle" },
      cellar: { east: "vault", up: "corridor" },
      vault: { west: "cellar" }
    },
    doors: {
      door: {
        connects: { study: "east", hall: "west" },
      isOpen: { value: 0, writable: true }
      },
      gate: {
        connects: { aisle: "east", cell: "west" },
        hasLock: "iron_key",
        isOpen: { value: 0, writable: true },
        isLocked: { value: 1, writable: true }
      },
      exit_gate: {
        connects: { lobby: "east" },
        hasLock: "silver_key",
        isOpen: { value: 0, writable: true },
        isLocked: { value: 1, writable: true }
      }
    },
    pitchBlackPlaces: ["cellar", "vault"]
  };

  //=========================================================
  // World objects
  things = {
    grapes: {
      locationInMap: { value: "cell", writable: true },
      amount: { value: 5, writable: true },
      nutritionalValue: 6,
      isEdible: 1,
      score: 1,
      canBePicked: 1
    },
    bottle: {
      locationInMap: { value: "cell", writable: true },
      contains: { value: "water", writable: true },
      amount: { value: 8, writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    guard_corpse: {
      locationInMap: { value: "aisle", writable: true },
      accessibleFrom: "cell",
      containerType: "forbidden",
      isContainer: 1
    },
    iron_key: {
      locationInMap: { value: "guard_corpse", writable: true },
      score: 5,
      canBePicked: 1
    },
    robe: {
      locationInMap: { value: "player", writable: true },
      canBePicked: 1,
      score: 1,
      wearAs: "onBody"
    },
    sandals: {
      locationInMap: { value: "player", writable: true },
      canBePicked: 1,
      score: 1,
      wearAs: "onFoot"
    },
    table: {
      locationInMap: { value: "restroom", writable: true },
      containerType: "over",
      isContainer: 1
    },
    cup: {
      locationInMap: { value: "table", writable: true },
      amount: { value: 1, writable: true },
      contains: { value: "wine", writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    jar: {
      locationInMap: { value: "table", writable: true },
      amount: { value: 5, writable: true },
      contains: { value: "wine", writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    drawer: {
      locationInMap: { value: "restroom", writable: true },
      containerType: "in",
      isContainer: 1,
      isOpen: { value: 0, writable: true }
    },
    lantern: {
      locationInMap: { value: "drawer", writable: true },
      score: 10,
      canBePicked: 1,
      isOn: { value: 0, writable: true },
      isLightSource: 1,
      timeOut: { value: 30, writable: true }
    },
    nail_bar: {
      locationInMap: { value: "drawer", writable: true },
      score: 3,
      canBePicked: 1
    },
    pack_of_cards: {
      locationInMap: { value: "drawer", writable: true },
      score: 1,
      canBePicked: 1
    },
    fork: {
      locationInMap: { value: "drawer", writable: true },
      score: 1,
      canBePicked: 1
    },
    rusted_key: {
      locationInMap: { value: "drawer", writable: true },
      score: 10,
      canBePicked: 1
    },
    dying_soldier: {
      locationInMap: { value: "restroom", writable: true },
    },
    soldier_corpse: {
      locationInMap: { value: "limbo", writable: true },
      containerType: "forbidden",
      isContainer: 1,
    },
    sword: {
      locationInMap: { value: "soldier_corpse", writable: true },
      score: 15,
      canBePicked: 1,
      isWeapon: 1
    },
    bread: {
      locationInMap: { value: "table", writable: true },
      score: 1,
      isEdible: 1,
      nutritionalValue: 4,
      amount: { value: 3, writable: true },
      canBePicked: 1
    },
    safe: {
      locationInMap: { value: "study", writable: true },
      containerType: "in",
      isContainer: 1,
      hasLock: "little_key",
      isLocked: { value: 1, writable: true },
      isOpen: { value: 0, writable: true }
    },
    gold_bars: {
      locationInMap: { value: "safe", writable: true },
      score: 50,
      canBePicked: 1
    },
    silver_key: {
      locationInMap: { value: "safe", writable: true },
      score: 30,
      canBePicked: 1
    },
    desk: {
      locationInMap: { value: "study", writable: true },
      containerType: "on",
      isContainer: 1
    },
    papers: {
      locationInMap: { value: "desk", writable: true },
      score: 20,
      canBePicked: 1
    },
    clock: {
      locationInMap: { value: "desk", writable: true },
      score: 1,
      canBePicked: 1
    },
    rack: {
      locationInMap: { value: "study", writable: true },
      containerType: "in",
      isContainer: 1
    },
    soldier_uniform: {
      locationInMap: { value: "rack", writable: true },
      score: 5,
      canBePicked: 1,
      wearAs: "onBody"
    },
    boots: {
      locationInMap: { value: "rack", writable: true },
      score: 5,
      canBePicked: 1,
      wearAs: "onFoot"
    },
    jackal: {
      locationInMap: { value: "vault", writable: true },
      alreadyDead: { value: 0, writable: true },
      hasSeenPlayer: { value: 0, writable: true }
    },
    jackal_corpse: {
      locationInMap: { value: "limbo", writable: true },
      alreadyDead: { value: 0, writable: true },
      hasSeenPlayer: { value: 0, writable: true }
    },
    chest: {
      locationInMap: { value: "vault", writable: true },
      containerType: "in",
      isContainer: 1,
      hasLock: "nonnexistent_key",
      isLocked: { value: 1, writable: true },
      isOpen: { value: 0, writable: true },
      isBrokeOpen: { value: 0, writable: true }
    },
    little_key: {
      locationInMap: { value: "chest", writable: true },
      score: 25,
      canBePicked: 1
    },
    gold_coins: {
      locationInMap: { value: "chest", writable: true },
      score: 20,
      canBePicked: 1
    },
    bag: {
      locationInMap: { value: "cellar", writable: true },
      score: 1,
      canBePicked: 1,
      containerType: "in",
      isContainer: 1
    },
    toolbox: {
      locationInMap: { value: "cellar", writable: true },
      containerType: "in",
      isContainer: 1
    },
    hammer: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    saw: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    wench: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    screwdriver: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    cask: {
      locationInMap: { value: "cellar", writable: true },
      contains: { value: "water", writable: true },
      amount: { value: 20, writable: true },
      isDrinkable: 1
    },
    painting: {
      locationInMap: { value: "lobby", writable: true },
      score: 15,
      canBePicked: 1
    },
    mirror: {
      locationInMap: { value: "hall", writable: true }
    }
  };

  //=========================================================
  // Player
  player = {
    locationInMap: { value: "cell", writable: true },
    health: { value: 6.5, writable: true },
    inventory: {
      rightHand: { value: undefined, writable: true },
      leftHand: { value: undefined, writable: true }
    },
    clothes: {
      onBody: { value: "robe", writable: true },
      onFoot: { value: "sandals", writable: true }
    }
  };
  
  //=========================================================
  // Aliases for shorter commands
  aliases = {
    n: "go north",
    s: "go south",
    e: "go east",
    w: "go west",
    u: "go up",
    d: "go down",
    i: "inventory"
  };

  //=========================================================
  // Game messages
  words = {
    open_gate_ok: [
      "You are free to roam. What now?"
    ],
    reach_aisle_ok: [
      "You hear a rattle from the west..."
    ],
    reach_restroom_ok: [
      "Something grabs your left foot. As you step back, frightened, and look",
      "down, you notice that the soldier is not dead yet. Cautiously, you  bend",
      "down towards him and hold his head up. He coughs blood, takes a deep breath,",
      "and says:",
      "- You... must... hurry. They... they are coming. The key... the key is in...",
      "the safe. Be careful... the... the vault... the jackal...",
      "He coughs blood again, mumbles unintelligibly, and dies."
    ],
    reach_cellar_ok: [
      "You hear an ominous roar from the east."
    ],
    killedJackalPitchBlack_ok: [
      "You fight desperately, guided only by the sound and your most basic",
      "survival instincts. In a lucky swing, you feel your sword cutting",
      "the flesh; then a howl of pain, then a thud on the floor...",
      "You stand still, in the complete darkness. Nothing happens."
    ],
    killedJackal_ok: [
      "The jackal attacks you fiercely, but you manage to stay away of his",
      "reach. Finally, after many attempts, you disembowel the beast with",
      "just one clean cut. The jackal, the guardian of the vault, is now",
      "dead."
    ],
    "GAMEOVER:timeUp": [
      "Suddenly, you hear a door broke open, and a mob of heavily armed men",
      "storms the building. While some of them pillage and vandalize the place,",
      "the others surround you. You don't stand a chance: down on your knees,",
      "you hear them speak in a language you don't recognize; then the ominous,",
      "metallic touch of the gun in your head. You close your eyes, and think no",
      "more."
    ],
    "GAMEOVER:dead": [
      "You are so weak you can barely breathe. As you pass out, you try to remember",
      "why are you here, what happened to you. But nothing comes to your mind, nothing",
      "but darkness, until you finally collapse."
    ],
    "GAMEOVER:deadFightingJackalPitchBlack": [
      "At least you died with your boots on, but fighting that beast in a dark",
      "basement was too disadvantageous. Bad luck this time..."
    ],
    "GAMEOVER:slainedByJackalPitchBlack": [
      "You stumbled across some ferocious animal that gave you no chance: in the blink",
      "of an eye, you were ripped apart with such force that you couldn't even scream.",
      "You suffered horribly in that last seconds, but at least died fast."
    ],
    "GAMEOVER:slainedByJackal": [
      "Now, the jackal unleashes all its strength and catches you off-guard: you watch",
      "in astonishment how easily the beast rips you in half, chipping your bones like",
      "sticks. When he reaches your neck and finally kills you, your last thoughts are,",
      "surprisingly, that the poor dog was in such a hunger that he deserves a good meal.",
      "Too bad it was you, but that's life."
    ],
    "GAMEOVER:exit": [
      "You have escaped the building! The sunlight hurts your eyes for a moment, but soon",
      "you can see a road that crosses a green field, towards a bridge over a wide river.",
      "You don't know exactly what happened, but it doesn't matter much now.",
      "You start your new journey, whistling carelessly against the breeze."
    ],
    "YOU_WIN!": [
      "Congratulations, you completed the quest!"
    ],
    cell: {
      title: "Cell",
      description: [
        "You are in a jail cell; a gate leads to the west."
      ]
    },
    aisle: {
      title: "Aisle",
      description: [
        "You are in an aisle, open to the west, east, and north."
      ]
    },
    restroom: {
      title: "Restroom",
      description: [
        "You are in a restroom, open only to the east."
      ]
    },
    corridor: {
      title: "Corridor",
      description: [
        "You are in a small corridor, open to the north and south. A hole in",
        "the ground reveals a ladder that leads down to the dark."
      ]
    },
    study: {
      title: "Study",
      description: [
        "The study."
      ]
    },
    hall: {
      title: "Hall",
      description: [
        "The main hall."
      ]
    },
    lobby: {
      title: "Lobby",
      description: [
        "A lobby."
      ]
    },
    cellar: {
      title: "Cellar",
      description: [
        "The cellar."
      ]
    },
    vault: {
      title: "Vault",
      description: [
        "The vault."
      ]
    },
    exit: {
      title: "Outside",
      description: [
        "You are free!!!"
      ]
    },
    grapes: {
      raw: "grapes",
      inspection: [
        "This is a nice bunch of grapes."
      ],
      here: "a bunch of grapes",
      consumed: "that was the last grape",
      denotative: "the grapes"
    },
    bottle: {
      raw: "bottle",
      inspection: [
        "A glass bottle."
      ],
      here: "a bottle",
      empty: "the bottle is empty",
      denotative: "the bottle"
    },
    water: {
      raw: "water"
    },
    wine: {
      raw: "wine"
    },
    guard_corpse: {
      raw: "guard corpse",
      inspection: [
        "A prison guard, judging by its uniform. He is most certainly dead;",
        "a sharp, deep wound crosses his back. He is lying face down just",
        "behind the gate, possibly within your reach."
      ],
      here: "the prison guard, dead on the floor",
      denotative: "the guard corpse",
      hasInside: "The corpse holds:",
      accessible: "A guard corpse, lying dead by the gate."
    },
    iron_key: {
      raw: "iron key",
      inspection: [
        "A big, long iron key."
      ],
      here: "an iron key",
      denotative: "the iron key"
    },
    gate: {
      raw: "gate",
      inspection: [
        "The gate is made of sturdy iron bars, and has a lock that opens with",
        "a rather big key."
      ],
      here: "a gate",
      denotative: "the gate"
    },
    table: {
      raw: "table",
      inspection: [
        "A wooden table, solid, hard, but rather worn out."
      ],
      denotative: "the table",
      hasInside: "On the table:",
      here: "a wooden table"
    },
    drawer: {
      raw: "drawer",
      inspection: [
        "An old and rusted office drawer."
      ],
      denotative: "the drawer",
      hasInside: "In the drawer:",
      here: "a drawer"
    },
    dying_soldier: {
      raw: "dying soldier",
      inspection: [
        "A soldier."
      ],
      denotative: "the soldier",
      here: "a soldier, lying on the floor, badly wounded"
    },
    soldier_corpse: {
      raw: "soldier corpse",
      inspection: [
        "The dead soldier is lying against the wall, covered in blood from a",
        "horrible wound in his neck. His clothes and bruises show visible signs",
        "of a vicious fight."
      ],
      denotative: "the soldier corpse",
      hasInside: "The soldier holds:",
      here: "a soldier, lying dead on the floor"
    },
    robe: {
      raw: "robe",
      inspection: [
        "This is a simple sackcloth robe."
      ],
      here: "a robe",
      denotative: "the robe"
    },
    sandals: {
      raw: "sandals",
      inspection: [
        "A pair of cheap and frayed leather sandals."
      ],
      here: "a pair of sandals",
      denotative: "the sandals"
    },
    cup: {
      raw: "cup",
      inspection: [
        "A ceramic mug, with a scratch in the handle; apparently it used to be red."
      ],
      here: "a cup",
      empty: "the cup is empty",
      denotative: "the cup"
    },
    jar: {
      raw: "jar",
      inspection: [
        "A glass jar."
      ],
      here: "a jar",
      empty: "the jar is empty",
      denotative: "the jar"
    },
    lantern: {
      raw: "lantern",
      inspection: [
        "A powerful flashlight, big and heavy; probably designed for military use."
      ],
      here: "a lantern",
      denotative: "the lantern"
    },
    nail_bar: {
      raw: "nail bar",
      inspection: [
        "A long, sturdy steel bar, flattened and sharpened in one end."
      ],
      here: "a nail bar",
      denotative: "the nail bar"
    },
    pack_of_cards: {
      raw: "pack of cards",
      inspection: [
        "A deck of poker cards."
      ],
      here: "a pack of cards",
      denotative: "the pack of cards"
    },
    fork: {
      raw: "fork",
      inspection: [
        "A kitchen fork, completely dull and scratched."
      ],
      here: "a fork",
      denotative: "the fork"
    },
    rusted_key: {
      raw: "rusted key",
      inspection: [
        "An iron key, rusted but otherwise unharmed; it appears to have been",
        "barely used, if at all."
      ],
      here: "a rusted key",
      denotative: "the rusted key"
    },
    sword: {
      raw: "sword",
      inspection: [
        "A long sword, somewhat worn out but extremely sharp. Seems to be a",
        "very efficient weapon, if you know how to use it."
      ],
      here: "a sword",
      denotative: "the sword"
    },
    bread: {
      raw: "bread",
      inspection: [
        "A piece of stale white bread. Not particularly good, but edible."
      ],
      here: "a piece of bread",
      consumed: "that was the last loaf of bread",
      denotative: "the piece of bread"
    },
    safe: {
      raw: "safe",
      inspection: [
        "A steel safe, probably impossible to open unless you have the key."
      ],
      here: "a safe",
      hasInside: "In the safe:",
      denotative: "the safe"
    },
    gold_bars: {
      raw: "gold bars",
      inspection: [
        "A quantity of gold bars; must be worth a fortune..."
      ],
      here: "some gold bars",
      denotative: "the gold bars"
    },
    silver_key: {
      raw: "silver key",
      inspection: [
        "A silver key, almost brand new."
      ],
      here: "a silver key",
      denotative: "the silver key"
    },
    desk: {
      raw: "desk",
      inspection: [
        "A lavish piece of furniture, made of exquisite wood."
      ],
      here: "a wooden desk",
      hasInside: "On the desk:",
      denotative: "the desk"
    },
    papers: {
      raw: "papers",
      inspection: [
        "A pile of papers; most of them have stamps, signatures and all signs",
        "of some kind of official or military matters."
      ],
      here: "a pile of papers",
      denotative: "the pile of papers"
    },
    clock: {
      raw: "clock",
      inspection: [
        "A luxurious wind-up clock."
      ],
      here: "a clock",
      denotative: "the clock"
    },
    rack: {
      raw: "rack",
      inspection: [
        "A finely crafted rack, made of wood and glass."
      ],
      here: "a rack",
      hasInside: "In the rack:",
      denotative: "the rack"
    },
    soldier_uniform: {
      raw: "soldier uniform",
      inspection: [
        "A soldier uniform, of the same colors and design as the one the prison",
        "guard was wearing, but undoubtedly of a higher rank, and perfectly clean",
        "and ironed."
      ],
      here: "a soldier uniform",
      denotative: "the soldier uniform"
    },
    boots: {
      raw: "boots",
      inspection: [
        "A fine pair of leather boots."
      ],
      here: "a pair of boots",
      denotative: "the boots"
    },
    jackal: {
      raw: "jackal",
      inspection: [
        "An enormous jackal stares at you, chained to a ring embedded in the stone",
        "wall. The beast pulls the chain with such fury and strength, it should break",
        "loose at any moment. Who knows how many days since his last meal..."
      ],
      here: "a jackal",
      denotative: "the jackal"
    },
    jackal_corpse: {
      raw: "jackal corpse",
      inspection: [
        "A dead jackal, brutally slained, lying over his own blood."
      ],
      here: "a dead jackal",
      denotative: "the jackal corpse"
    },
    chest: {
      raw: "chest",
      inspection: [
        "A big wooden chest. It has two rusty locks in the front side."
      ],
      here: "a chest",
      hasInside: "In the chest:",
      denotative: "the chest"
    },
    little_key: {
      raw: "little key",
      inspection: [
        "A little round key, in very good state."
      ],
      here: "a little key",
      denotative: "the little key"
    },
    gold_coins: {
      raw: "gold coins",
      inspection: [
        "A bunch of gold coins, not too many, but probably of some value."
      ],
      here: "a bunch of gold coins",
      denotative: "the gold coins"
    },
    bag: {
      raw: "bag",
      inspection: [
        "A big sackcloth bag."
      ],
      here: "a bag",
      hasInside: "In the bag:",
      denotative: "the bag"
    },
    toolbox: {
      raw: "toolbox",
      inspection: [
        "An old and rusted toolbox, with no lid."
      ],
      here: "a toolbox",
      hasInside: "In the toolbox:",
      denotative: "the toolbox"
    },
    hammer: {
      raw: "hammer",
      inspection: [
        "A heavy, sturdy blacksmith hammer."
      ],
      here: "a hammer",
      denotative: "the hammer"
    },
    saw: {
      raw: "saw",
      inspection: [
        "An old saw, dull and slack, probably unusable."
      ],
      here: "a saw",
      denotative: "the saw"
    },
    wench: {
      raw: "wench",
      inspection: [
        "A big steel wench."
      ],
      here: "a wench",
      denotative: "the wench"
    },
    screwdriver: {
      raw: "screwdriver",
      inspection: [
        "A screwdriver so worn out that seems to be barely useful for anything."
      ],
      here: "a screwdriver",
      denotative: "the screwdriver"
    },
    cask: {
      raw: "cask",
      inspection: [
        "A big, round ceramic pot."
      ],
      here: "a cask",
      denotative: "the cask"
    },
    painting: {
      raw: "painting",
      inspection: [
        "A beautiful reproduction of a ship sailing through a storm."
      ],
      here: "a painting",
      denotative: "the painting"
    },
    mirror: {
      raw: "mirror",
      inspection: [
        "A wall mirror with a very finely crafted frame."
      ],
      here: "a mirror",
      denotative: "the mirror"
    },
    door: {
      raw: "door",
      inspection: [
        "An elegant, expensive door, made of hard wood."
      ],
      here: "a door",
      denotative: "the door"
    },
    exit_gate: {
      raw: "exit gate",
      inspection: [
        "A solid steel gate, probably very thick. It seems to be harder and",
        "heavier than the walls themselves."
      ],
      here: "the exit gate",
      denotative: "the exit gate"
    },
    GAME_START: [
      "<b>VAULT</b>",
      "An Interactive Fiction Original",
      "By spiridon (2022)",
      "NanoFiction Release 1.0",
      "",
      "You wake up in some sort of jail cell. As you  try  to  open  your  eyes,",
      "the bright light almost  makes  you  scream  in  pain.  Your  head  aches",
      "horribly, your face is covered with blood.",
      "As you slowly regain consciousness, you notice that  you  are  wearing  a",
      "dirty robe and a pair of  leather  sandals;  the  marks  in  your  wrists",
      "indicate that you must have been brought here in chains. Last  thing  you",
      "remember, you were running in a dark alley, trying to escape (from who?),",
      "then a sudden blow in the back of your head, then nothing but darkness.",
      "As you struggle to stand up, you notice how weak, hungry and thirsty  you",
      "feel."
    ],
    BAD_SYNTAX: ["That sentence doesn't make sense."],
    COMMAND_NOT_FOUND: ["Nothing happens."],
    GO_WHERE: ["Go where?"],
    NOT_FOUND: ["Not found."],
    CANNOT_GO_THERE: ["You cannot go there."],
    UNLOCKING_IT: ["Unlocked."],
    LOCKING_IT: ["Locked."],
    NEED_THE_KEY: ["You don't have the key."],
    IT_IS_LOCKED: ["It is locked."],
    ALREADY_LOCKED: ["It is already locked."],
    ALREADY_CLOSE: ["It is closed already."],
    OPENING_IT: ["Open."],
    ALREADY_OPEN: ["It is already open."],
    ALREADY_UNLOCKED: ["It is unlocked already."],
    CANNOT_BE_CLOSED: ["You can't close that."],
    CANNOT_BE_LOCKED: ["Yo can't lock that."],
    CANNOT_BE_OPENED: ["You can't open that."],
    CANNOT_BE_UNLOCKED: ["There's nothing to unlock."],
    CANNOT_DROP: ["You can't do that."],
    CANNOT_PICK_THAT: ["You can't pick that."],
    CANNOT_WEAR_THAT: ["You can't wear that."],
    CLOSE_WHAT: ["Close what?"],
    CLOSING_IT: ["Closed."],
    CONTAINER_NOT_HERE: ["Can't find it..."],
    DONE: ["Done."],
    DOOR_ISNT_HERE: ["Can't find it."],
    DRINK_WHAT: ["Drink what?"],
    feelAwesome: ["You feel really good."],
    feelBetter: ["You feel better."],
    feelBetterButIll: ["You don't feel that bad, but still..."],
    feelBetterButStarving: ["You feel better, but still starving."],
    feelIll: ["You start to feel weak."],
    feelNotTooGood: ["You feel a little dizzy."],
    feelStarving: ["You are starving!"],
    ITS_NOT_HERE: ["Can't find that."],
    LANTERN_ALREADY_DEAD: ["The lantern is already dead."],
    LANTERN_ALREADY_OFF: ["It is already off."],
    LANTERN_ALREADY_ON: ["It is on already."],
    LANTERN_IS_ABOUT_TO_DIE: ["The lantern is about to die."],
    LANTERN_IS_DEAD: ["The lantern is dead."],
    LANTERN_IS_NOW_OFF: ["Lantern goes off."],
    LANTERN_IS_NOW_ON: ["The lantern is on."],
    LOCK_WHAT: ["Lock what?"],
    NO_AVAILABLE_SLOTS: ["You can't carry any more items."],
    NO_AVAILABLE_SLOTS_FOR_CLOTHES: ["You are wearing something already."],
    NOT_A_CONTAINER: ["You can't put it there."],
    NOT_EDIBLE: ["You cannot eat that."],
    NOT_IN_INVENTORY: ["You don't have it."],
    OPENING_CHEST: ["*** THE CHEST IS OPEN ***"],
    OPENING_IT: ["Open."],
    OPEN_WHAT: ["Open what?"],
    PHRASED_INCORRECTLY: ["Can't understand that phrase."],
    PITCH_BLACK: ["It is pitch black; you can't see a thing."],
    PUT_WHAT: ["Put what?"],
    SLURP: ["You drink a good sip."],
    UNLOCK_WHAT: ["Unlock what?"],
    YOU_DONT_HAVE_THE_KEY: ["You need the key."],
    YOU_LOSE: ["*** YOU LOSE ***"],
    YOU_TRY_BUT_FAIL: ["You try, but fail."],
    YUMMY: ["Yummy!"],
    HELP: [
      "Available commands:",
      "go direction (up down east west north south); aliases: u d e w n s",
      "pick X, drop X, wear X, put X in|on|over Y, eat X, drink X",
      "look, inspect X, open X, close X, lock X, unlock X",
      "lantern on|off"
    ]
  };

  //=========================================================
  // References for two-word names
  refs = {
    "little key": "little_key",
    "soldier uniform": "soldier_uniform",
    "gold bars": "gold_bars",
    "silver key": "silver_key",
    "soldier corpse": "soldier_corpse",
    "rusted key": "rusted_key",
    "pack of cards": "pack_of_cards",
    "nail bar": "nail_bar",
    "exit gate": "exit_gate",
    "guard corpse": "guard_corpse",
    "iron key": "iron_key"
  };

  //=========================================================
  // Game state
  state = {
    score: { value: 0, writable: true },
    gameState: { value: "playing", writable: true },
    timeLimit: 100,
    elapsedTime: { value: 0, writable: true },
    jackalAware: { value: 0, writable: true },
  };
  
  // Get ready
  getReady();
}

// Start a new game when we load
window.onload = newgame;

