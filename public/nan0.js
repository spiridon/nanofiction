/*
 *  NANOFICTION v1.0
 */

'use strict';

// Helper function (unused, save for later)
function includeFile(path, callback)
{
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.async = true;
  script.src = path;
  script.onload = function() {
    if(typeof(callback) == "function")
    {
      callback();
    }
  }
  try
  {
    var scriptOne = document.getElementsByTagName('script')[0];
    scriptOne.parentNode.insertBefore(script, scriptOne);
  }
  catch(e)
  {
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}

/*
 *  HELPER FUNCTIONS
 */

// Scroll right to the bottom of the document
function scrollDown() {
  // Scroll down
  var scrollHeight = document.body.scrollHeight;
  window.scrollTo(0, scrollHeight);
}

function waitForInput() {
  ui.input.style.visibility = "visible";
  ui.input.value = "";
  ui.input.focus();
  scrollDown();
}

function stripFirst(a) {
  //return a.reverse().pop().reverse();
  var t = a.reverse();
  t.pop();
  return t.reverse();
}

/*
 *  GAME INITIALIZATION
 */

// Initialize
function newgame() {
  // Set up an object to hold document elements we care about
  ui = {
    scoreboard: null,
    message: null,  // The game response.
    input: null,     // Where the user enters the commands.
    userinput: null
  };
  
  // Initialize each of these element ids
  for(var id in ui) ui[id] = document.getElementById(id);
    
  // Define an event handler for the input field
  ui.input.onchange = handleInput;

  //=========================================================
  // Milestones
  milestones = {
    open_gate: { score: 5, done: 0 },
    reach_aisle: { score: 0, done: 0 },
    reach_restroom: { score: 10, done: 0 },
    reach_cellar: { score: 0, done: 0 },
    reach_vault: { score: 15, done: 0 },
    killedJackal: { score: 60, done: 0 },
    killedJackalPitchBlack: { score: 75, done: 0 },
    jackalHasAwakened: { score: 7, done: 0 },
    open_chest: { score: 20, done: 0 },
    open_safe: { score: 30, done: 0 },
    reach_exit: { score: 100, done: 0 }
  };

  //=========================================================
  // World
  map = {
    pathways: {
      study: { east: "hall" },
      hall: { west: "study", east: "lobby", south: "corridor" },
      lobby: { west: "hall", east: "exit" },
      corridor: { north: "hall", south: "aisle", down: "cellar" },
      restroom: { east: "aisle" },
      aisle: { west: "restroom", east: "cell", north: "corridor" },
      cell: { west: "aisle" },
      cellar: { east: "vault", up: "corridor" },
      vault: { west: "cellar" }
    },
    doors: {
      door: {
        connects: { study: "east", hall: "west" },
      isOpen: { value: 0, writable: true }
      },
      gate: {
        connects: { aisle: "east", cell: "west" },
        hasLock: "iron_key",
        isOpen: { value: 0, writable: true },
        isLocked: { value: 1, writable: true }
      },
      exit_gate: {
        connects: { lobby: "east" },
        hasLock: "silver_key",
        isOpen: { value: 0, writable: true },
        isLocked: { value: 1, writable: true }
      }
    },
    pitchBlackPlaces: ["cellar", "vault"]
  };

  //=========================================================
  // World objects
  things = {
    grapes: {
      locationInMap: { value: "cell", writable: true },
      amount: { value: 5, writable: true },
      nutritionalValue: 6,
      isEdible: 1,
      score: 1,
      canBePicked: 1
    },
    bottle: {
      locationInMap: { value: "cell", writable: true },
      contains: { value: "water", writable: true },
      amount: { value: 8, writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    guard_corpse: {
      locationInMap: { value: "aisle", writable: true },
      accessibleFrom: "cell",
      containerType: "forbidden",
      isContainer: 1
    },
    iron_key: {
      locationInMap: { value: "guard_corpse", writable: true },
      score: 5,
      canBePicked: 1
    },
    robe: {
      locationInMap: { value: "player", writable: true },
      canBePicked: 1,
      score: 1,
      wearAs: "onBody"
    },
    sandals: {
      locationInMap: { value: "player", writable: true },
      canBePicked: 1,
      score: 1,
      wearAs: "onFoot"
    },
    table: {
      locationInMap: { value: "restroom", writable: true },
      containerType: "over",
      isContainer: 1
    },
    cup: {
      locationInMap: { value: "table", writable: true },
      amount: { value: 1, writable: true },
      contains: { value: "wine", writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    jar: {
      locationInMap: { value: "table", writable: true },
      amount: { value: 5, writable: true },
      contains: { value: "wine", writable: true },
      isDrinkable: 1,
      canBePicked: 1,
      score: 1,
      canBeRefilled: 1
    },
    drawer: {
      locationInMap: { value: "restroom", writable: true },
      containerType: "in",
      isContainer: 1,
      isOpen: { value: 0, writable: true }
    },
    lantern: {
      locationInMap: { value: "drawer", writable: true },
      score: 10,
      canBePicked: 1,
      isOn: { value: 0, writable: true },
      isLightSource: 1,
      timeOut: { value: 30, writable: true }
    },
    nail_bar: {
      locationInMap: { value: "drawer", writable: true },
      score: 3,
      canBePicked: 1
    },
    pack_of_cards: {
      locationInMap: { value: "drawer", writable: true },
      score: 1,
      canBePicked: 1
    },
    fork: {
      locationInMap: { value: "drawer", writable: true },
      score: 1,
      canBePicked: 1
    },
    rusted_key: {
      locationInMap: { value: "drawer", writable: true },
      score: 10,
      canBePicked: 1
    },
    dying_soldier: {
      locationInMap: { value: "restroom", writable: true },
    },
    soldier_corpse: {
      locationInMap: { value: "limbo", writable: true },
      containerType: "forbidden",
      isContainer: 1,
    },
    sword: {
      locationInMap: { value: "soldier_corpse", writable: true },
      score: 15,
      canBePicked: 1,
      isWeapon: 1
    },
    bread: {
      locationInMap: { value: "table", writable: true },
      score: 1,
      isEdible: 1,
      nutritionalValue: 4,
      amount: { value: 3, writable: true },
      canBePicked: 1
    },
    safe: {
      locationInMap: { value: "study", writable: true },
      containerType: "in",
      isContainer: 1,
      hasLock: "little_key",
      isLocked: { value: 1, writable: true },
      isOpen: { value: 0, writable: true }
    },
    gold_bars: {
      locationInMap: { value: "safe", writable: true },
      score: 50,
      canBePicked: 1
    },
    silver_key: {
      locationInMap: { value: "safe", writable: true },
      score: 30,
      canBePicked: 1
    },
    desk: {
      locationInMap: { value: "study", writable: true },
      containerType: "on",
      isContainer: 1
    },
    papers: {
      locationInMap: { value: "desk", writable: true },
      score: 20,
      canBePicked: 1
    },
    clock: {
      locationInMap: { value: "desk", writable: true },
      score: 1,
      canBePicked: 1
    },
    rack: {
      locationInMap: { value: "study", writable: true },
      containerType: "in",
      isContainer: 1
    },
    soldier_uniform: {
      locationInMap: { value: "rack", writable: true },
      score: 5,
      canBePicked: 1,
      wearAs: "onBody"
    },
    boots: {
      locationInMap: { value: "rack", writable: true },
      score: 5,
      canBePicked: 1,
      wearAs: "onFoot"
    },
    jackal: {
      locationInMap: { value: "vault", writable: true },
      alreadyDead: { value: 0, writable: true },
      hasSeenPlayer: { value: 0, writable: true }
    },
    jackal_corpse: {
      locationInMap: { value: "limbo", writable: true },
      alreadyDead: { value: 0, writable: true },
      hasSeenPlayer: { value: 0, writable: true }
    },
    chest: {
      locationInMap: { value: "vault", writable: true },
      containerType: "in",
      isContainer: 1,
      hasLock: "nonnexistent_key",
      isLocked: { value: 1, writable: true },
      isOpen: { value: 0, writable: true },
      isBrokeOpen: { value: 0, writable: true }
    },
    little_key: {
      locationInMap: { value: "chest", writable: true },
      score: 25,
      canBePicked: 1
    },
    gold_coins: {
      locationInMap: { value: "chest", writable: true },
      score: 20,
      canBePicked: 1
    },
    bag: {
      locationInMap: { value: "cellar", writable: true },
      score: 1,
      canBePicked: 1,
      containerType: "in",
      isContainer: 1
    },
    toolbox: {
      locationInMap: { value: "cellar", writable: true },
      containerType: "in",
      isContainer: 1
    },
    hammer: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    saw: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    wench: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    screwdriver: {
      locationInMap: { value: "toolbox", writable: true },
      score: 1,
      canBePicked: 1
    },
    cask: {
      locationInMap: { value: "cellar", writable: true },
      contains: { value: "water", writable: true },
      amount: { value: 20, writable: true },
      isDrinkable: 1
    },
    painting: {
      locationInMap: { value: "lobby", writable: true },
      score: 15,
      canBePicked: 1
    },
    mirror: {
      locationInMap: { value: "hall", writable: true }
    }
  };

  //=========================================================
  // Player
  player = {
    locationInMap: { value: "cell", writable: true },
    health: { value: 6.5, writable: true },
    inventory: {
      rightHand: { value: undefined, writable: true },
      leftHand: { value: undefined, writable: true }
    },
    clothes: {
      onBody: { value: "robe", writable: true },
      onFoot: { value: "sandals", writable: true }
    }
  };
  
  //=========================================================
  // Aliases for shorter commands
  aliases = {
    n: "go north",
    s: "go south",
    e: "go east",
    w: "go west",
    u: "go up",
    d: "go down",
    i: "inventory"
  };

  //=========================================================
  // Game messages
  words = {
    open_gate_ok: [
      "You are free to roam. What now?"
    ],
    reach_aisle_ok: [
      "You hear a rattle from the west..."
    ],
    reach_restroom_ok: [
      "Something grabs your left foot. As you step back, frightened, and look",
      "down, you notice that the soldier is not dead yet. Cautiously, you  bend",
      "down towards him and hold his head up. He coughs blood, takes a deep breath,",
      "and says:",
      "- You... must... hurry. They... they are coming. The key... the key is in...",
      "the safe. Be careful... the... the vault... the jackal...",
      "He coughs blood again, mumbles unintelligibly, and dies."
    ],
    reach_cellar_ok: [
      "You hear an ominous roar from the east."
    ],
    killedJackalPitchBlack_ok: [
      "You fight desperately, guided only by the sound and your most basic",
      "survival instincts. In a lucky swing, you feel your sword cutting",
      "the flesh; then a howl of pain, then a thud on the floor...",
      "You stand still, in the complete darkness. Nothing happens."
    ],
    killedJackal_ok: [
      "The jackal attacks you fiercely, but you manage to stay away of his",
      "reach. Finally, after many attempts, you disembowel the beast with",
      "just one clean cut. The jackal, the guardian of the vault, is now",
      "dead."
    ],
    "GAMEOVER:timeUp": [
      "Suddenly, you hear a door broke open, and a mob of heavily armed men",
      "storms the building. While some of them pillage and vandalize the place,",
      "the others surround you. You don't stand a chance: down on your knees,",
      "you hear them speak in a language you don't recognize; then the ominous,",
      "metallic touch of the gun in your head. You close your eyes, and think no",
      "more."
    ],
    "GAMEOVER:dead": [
      "You are so weak you can barely breathe. As you pass out, you try to remember",
      "why are you here, what happened to you. But nothing comes to your mind, nothing",
      "but darkness, until you finally collapse."
    ],
    "GAMEOVER:deadFightingJackalPitchBlack": [
      "At least you died with your boots on, but fighting that beast in a dark",
      "basement was too disadvantageous. Bad luck this time..."
    ],
    "GAMEOVER:slainedByJackalPitchBlack": [
      "You stumbled across some ferocious animal that gave you no chance: in the blink",
      "of an eye, you were ripped apart with such force that you couldn't even scream.",
      "You suffered horribly in that last seconds, but at least died fast."
    ],
    "GAMEOVER:slainedByJackal": [
      "Now, the jackal unleashes all its strength and catches you off-guard: you watch",
      "in astonishment how easily the beast rips you in half, chipping your bones like",
      "sticks. When he reaches your neck and finally kills you, your last thoughts are,",
      "surprisingly, that the poor dog was in such a hunger that he deserves a good meal.",
      "Too bad it was you, but that's life."
    ],
    "GAMEOVER:exit": [
      "You have escaped the building! The sunlight hurts your eyes for a moment, but soon",
      "you can see a road that crosses a green field, towards a bridge over a wide river.",
      "You don't know exactly what happened, but it doesn't matter much now.",
      "You start your new journey, whistling carelessly against the breeze."
    ],
    "YOU_WIN!": [
      "Congratulations, you completed the quest!"
    ],
    cell: {
      title: "Cell",
      description: [
        "You are in a jail cell; a gate leads to the west."
      ]
    },
    aisle: {
      title: "Aisle",
      description: [
        "You are in an aisle, open to the west, east, and north."
      ]
    },
    restroom: {
      title: "Restroom",
      description: [
        "You are in a restroom, open only to the east."
      ]
    },
    corridor: {
      title: "Corridor",
      description: [
        "You are in a small corridor, open to the north and south. A hole in",
        "the ground reveals a ladder that leads down to the dark."
      ]
    },
    study: {
      title: "Study",
      description: [
        "The study."
      ]
    },
    hall: {
      title: "Hall",
      description: [
        "The main hall."
      ]
    },
    lobby: {
      title: "Lobby",
      description: [
        "A lobby."
      ]
    },
    cellar: {
      title: "Cellar",
      description: [
        "The cellar."
      ]
    },
    vault: {
      title: "Vault",
      description: [
        "The vault."
      ]
    },
    exit: {
      title: "Outside",
      description: [
        "You are free!!!"
      ]
    },
    grapes: {
      raw: "grapes",
      inspection: [
        "This is a nice bunch of grapes."
      ],
      here: "a bunch of grapes",
      consumed: "that was the last grape",
      denotative: "the grapes"
    },
    bottle: {
      raw: "bottle",
      inspection: [
        "A glass bottle."
      ],
      here: "a bottle",
      empty: "the bottle is empty",
      denotative: "the bottle"
    },
    water: {
      raw: "water"
    },
    wine: {
      raw: "wine"
    },
    guard_corpse: {
      raw: "guard corpse",
      inspection: [
        "A prison guard, judging by its uniform. He is most certainly dead;",
        "a sharp, deep wound crosses his back. He is lying face down just",
        "behind the gate, possibly within your reach."
      ],
      here: "the prison guard, dead on the floor",
      denotative: "the guard corpse",
      hasInside: "The corpse holds:",
      accessible: "A guard corpse, lying dead by the gate."
    },
    iron_key: {
      raw: "iron key",
      inspection: [
        "A big, long iron key."
      ],
      here: "an iron key",
      denotative: "the iron key"
    },
    gate: {
      raw: "gate",
      inspection: [
        "The gate is made of sturdy iron bars, and has a lock that opens with",
        "a rather big key."
      ],
      here: "a gate",
      denotative: "the gate"
    },
    table: {
      raw: "table",
      inspection: [
        "A wooden table, solid, hard, but rather worn out."
      ],
      denotative: "the table",
      hasInside: "On the table:",
      here: "a wooden table"
    },
    drawer: {
      raw: "drawer",
      inspection: [
        "An old and rusted office drawer."
      ],
      denotative: "the drawer",
      hasInside: "In the drawer:",
      here: "a drawer"
    },
    dying_soldier: {
      raw: "dying soldier",
      inspection: [
        "A soldier."
      ],
      denotative: "the soldier",
      here: "a soldier, lying on the floor, badly wounded"
    },
    soldier_corpse: {
      raw: "soldier corpse",
      inspection: [
        "The dead soldier is lying against the wall, covered in blood from a",
        "horrible wound in his neck. His clothes and bruises show visible signs",
        "of a vicious fight."
      ],
      denotative: "the soldier corpse",
      hasInside: "The soldier holds:",
      here: "a soldier, lying dead on the floor"
    },
    robe: {
      raw: "robe",
      inspection: [
        "This is a simple sackcloth robe."
      ],
      here: "a robe",
      denotative: "the robe"
    },
    sandals: {
      raw: "sandals",
      inspection: [
        "A pair of cheap and frayed leather sandals."
      ],
      here: "a pair of sandals",
      denotative: "the sandals"
    },
    cup: {
      raw: "cup",
      inspection: [
        "A ceramic mug, with a scratch in the handle; apparently it used to be red."
      ],
      here: "a cup",
      empty: "the cup is empty",
      denotative: "the cup"
    },
    jar: {
      raw: "jar",
      inspection: [
        "A glass jar."
      ],
      here: "a jar",
      empty: "the jar is empty",
      denotative: "the jar"
    },
    lantern: {
      raw: "lantern",
      inspection: [
        "A powerful flashlight, big and heavy; probably designed for military use."
      ],
      here: "a lantern",
      denotative: "the lantern"
    },
    nail_bar: {
      raw: "nail bar",
      inspection: [
        "A long, sturdy steel bar, flattened and sharpened in one end."
      ],
      here: "a nail bar",
      denotative: "the nail bar"
    },
    pack_of_cards: {
      raw: "pack of cards",
      inspection: [
        "A deck of poker cards."
      ],
      here: "a pack of cards",
      denotative: "the pack of cards"
    },
    fork: {
      raw: "fork",
      inspection: [
        "A kitchen fork, completely dull and scratched."
      ],
      here: "a fork",
      denotative: "the fork"
    },
    rusted_key: {
      raw: "rusted key",
      inspection: [
        "An iron key, rusted but otherwise unharmed; it appears to have been",
        "barely used, if at all."
      ],
      here: "a rusted key",
      denotative: "the rusted key"
    },
    sword: {
      raw: "sword",
      inspection: [
        "A long sword, somewhat worn out but extremely sharp. Seems to be a",
        "very efficient weapon, if you know how to use it."
      ],
      here: "a sword",
      denotative: "the sword"
    },
    bread: {
      raw: "bread",
      inspection: [
        "A piece of stale white bread. Not particularly good, but edible."
      ],
      here: "a piece of bread",
      consumed: "that was the last loaf of bread",
      denotative: "the piece of bread"
    },
    safe: {
      raw: "safe",
      inspection: [
        "A steel safe, probably impossible to open unless you have the key."
      ],
      here: "a safe",
      hasInside: "In the safe:",
      denotative: "the safe"
    },
    gold_bars: {
      raw: "gold bars",
      inspection: [
        "A quantity of gold bars; must be worth a fortune..."
      ],
      here: "some gold bars",
      denotative: "the gold bars"
    },
    silver_key: {
      raw: "silver key",
      inspection: [
        "A silver key, almost brand new."
      ],
      here: "a silver key",
      denotative: "the silver key"
    },
    desk: {
      raw: "desk",
      inspection: [
        "A lavish piece of furniture, made of exquisite wood."
      ],
      here: "a wooden desk",
      hasInside: "On the desk:",
      denotative: "the desk"
    },
    papers: {
      raw: "papers",
      inspection: [
        "A pile of papers; most of them have stamps, signatures and all signs",
        "of some kind of official or military matters."
      ],
      here: "a pile of papers",
      denotative: "the pile of papers"
    },
    clock: {
      raw: "clock",
      inspection: [
        "A luxurious wind-up clock."
      ],
      here: "a clock",
      denotative: "the clock"
    },
    rack: {
      raw: "rack",
      inspection: [
        "A finely crafted rack, made of wood and glass."
      ],
      here: "a rack",
      hasInside: "In the rack:",
      denotative: "the rack"
    },
    soldier_uniform: {
      raw: "soldier uniform",
      inspection: [
        "A soldier uniform, of the same colors and design as the one the prison",
        "guard was wearing, but undoubtedly of a higher rank, and perfectly clean",
        "and ironed."
      ],
      here: "a soldier uniform",
      denotative: "the soldier uniform"
    },
    boots: {
      raw: "boots",
      inspection: [
        "A fine pair of leather boots."
      ],
      here: "a pair of boots",
      denotative: "the boots"
    },
    jackal: {
      raw: "jackal",
      inspection: [
        "An enormous jackal stares at you, chained to a ring embedded in the stone",
        "wall. The beast pulls the chain with such fury and strength, it should break",
        "loose at any moment. Who knows how many days since his last meal..."
      ],
      here: "a jackal",
      denotative: "the jackal"
    },
    jackal_corpse: {
      raw: "jackal corpse",
      inspection: [
        "A dead jackal, brutally slained, lying over his own blood."
      ],
      here: "a dead jackal",
      denotative: "the jackal corpse"
    },
    chest: {
      raw: "chest",
      inspection: [
        "A big wooden chest. It has two rusty locks in the front side."
      ],
      here: "a chest",
      hasInside: "In the chest:",
      denotative: "the chest"
    },
    little_key: {
      raw: "little key",
      inspection: [
        "A little round key, in very good state."
      ],
      here: "a little key",
      denotative: "the little key"
    },
    gold_coins: {
      raw: "gold coins",
      inspection: [
        "A bunch of gold coins, not too many, but probably of some value."
      ],
      here: "a bunch of gold coins",
      denotative: "the gold coins"
    },
    bag: {
      raw: "bag",
      inspection: [
        "A big sackcloth bag."
      ],
      here: "a bag",
      hasInside: "In the bag:",
      denotative: "the bag"
    },
    toolbox: {
      raw: "toolbox",
      inspection: [
        "An old and rusted toolbox, with no lid."
      ],
      here: "a toolbox",
      hasInside: "In the toolbox:",
      denotative: "the toolbox"
    },
    hammer: {
      raw: "hammer",
      inspection: [
        "A heavy, sturdy blacksmith hammer."
      ],
      here: "a hammer",
      denotative: "the hammer"
    },
    saw: {
      raw: "saw",
      inspection: [
        "An old saw, dull and slack, probably unusable."
      ],
      here: "a saw",
      denotative: "the saw"
    },
    wench: {
      raw: "wench",
      inspection: [
        "A big steel wench."
      ],
      here: "a wench",
      denotative: "the wench"
    },
    screwdriver: {
      raw: "screwdriver",
      inspection: [
        "A screwdriver so worn out that seems to be barely useful for anything."
      ],
      here: "a screwdriver",
      denotative: "the screwdriver"
    },
    cask: {
      raw: "cask",
      inspection: [
        "A big, round ceramic pot."
      ],
      here: "a cask",
      denotative: "the cask"
    },
    painting: {
      raw: "painting",
      inspection: [
        "A beautiful reproduction of a ship sailing through a storm."
      ],
      here: "a painting",
      denotative: "the painting"
    },
    mirror: {
      raw: "mirror",
      inspection: [
        "A wall mirror with a very finely crafted frame."
      ],
      here: "a mirror",
      denotative: "the mirror"
    },
    door: {
      raw: "door",
      inspection: [
        "An elegant, expensive door, made of hard wood."
      ],
      here: "a door",
      denotative: "the door"
    },
    exit_gate: {
      raw: "exit gate",
      inspection: [
        "A solid steel gate, probably very thick. It seems to be harder and",
        "heavier than the walls themselves."
      ],
      here: "the exit gate",
      denotative: "the exit gate"
    },
    GAME_START: [
      "<b>VAULT</b>",
      "An Interactive Fiction Original",
      "By spiridon (2022)",
      "NanoFiction Release 1.0",
      "",
      "You wake up in some sort of jail cell. As you  try  to  open  your  eyes,",
      "the bright light almost  makes  you  scream  in  pain.  Your  head  aches",
      "horribly, your face is covered with blood.",
      "As you slowly regain consciousness, you notice that  you  are  wearing  a",
      "dirty robe and a pair of  leather  sandals;  the  marks  in  your  wrists",
      "indicate that you must have been brought here in chains. Last  thing  you",
      "remember, you were running in a dark alley, trying to escape (from who?),",
      "then a sudden blow in the back of your head, then nothing but darkness.",
      "As you struggle to stand up, you notice how weak, hungry and thirsty  you",
      "feel."
    ],
    BAD_SYNTAX: ["That sentence doesn't make sense."],
    COMMAND_NOT_FOUND: ["Nothing happens."],
    GO_WHERE: ["Go where?"],
    NOT_FOUND: ["Not found."],
    CANNOT_GO_THERE: ["You cannot go there."],
    UNLOCKING_IT: ["Unlocked."],
    LOCKING_IT: ["Locked."],
    NEED_THE_KEY: ["You don't have the key."],
    IT_IS_LOCKED: ["It is locked."],
    ALREADY_LOCKED: ["It is already locked."],
    ALREADY_CLOSE: ["It is closed already."],
    OPENING_IT: ["Open."],
    ALREADY_OPEN: ["It is already open."],
    ALREADY_UNLOCKED: ["It is unlocked already."],
    CANNOT_BE_CLOSED: ["You can't close that."],
    CANNOT_BE_LOCKED: ["Yo can't lock that."],
    CANNOT_BE_OPENED: ["You can't open that."],
    CANNOT_BE_UNLOCKED: ["There's nothing to unlock."],
    CANNOT_DROP: ["You can't do that."],
    CANNOT_PICK_THAT: ["You can't pick that."],
    CANNOT_WEAR_THAT: ["You can't wear that."],
    CLOSE_WHAT: ["Close what?"],
    CLOSING_IT: ["Closed."],
    CONTAINER_NOT_HERE: ["Can't find it..."],
    DONE: ["Done."],
    DOOR_ISNT_HERE: ["Can't find it."],
    DRINK_WHAT: ["Drink what?"],
    feelAwesome: ["You feel really good."],
    feelBetter: ["You feel better."],
    feelBetterButIll: ["You don't feel that bad, but still..."],
    feelBetterButStarving: ["You feel better, but still starving."],
    feelIll: ["You start to feel weak."],
    feelNotTooGood: ["You feel a little dizzy."],
    feelStarving: ["You are starving!"],
    ITS_NOT_HERE: ["Can't find that."],
    LANTERN_ALREADY_DEAD: ["The lantern is already dead."],
    LANTERN_ALREADY_OFF: ["It is already off."],
    LANTERN_ALREADY_ON: ["It is on already."],
    LANTERN_IS_ABOUT_TO_DIE: ["The lantern is about to die."],
    LANTERN_IS_DEAD: ["The lantern is dead."],
    LANTERN_IS_NOW_OFF: ["Lantern goes off."],
    LANTERN_IS_NOW_ON: ["The lantern is on."],
    LOCK_WHAT: ["Lock what?"],
    NO_AVAILABLE_SLOTS: ["You can't carry any more items."],
    NO_AVAILABLE_SLOTS_FOR_CLOTHES: ["You are wearing something already."],
    NOT_A_CONTAINER: ["You can't put it there."],
    NOT_EDIBLE: ["You cannot eat that."],
    NOT_IN_INVENTORY: ["You don't have it."],
    OPENING_CHEST: ["*** THE CHEST IS OPEN ***"],
    OPENING_IT: ["Open."],
    OPEN_WHAT: ["Open what?"],
    PHRASED_INCORRECTLY: ["Can't understand that phrase."],
    PITCH_BLACK: ["It is pitch black; you can't see a thing."],
    PUT_WHAT: ["Put what?"],
    SLURP: ["You drink a good sip."],
    UNLOCK_WHAT: ["Unlock what?"],
    YOU_DONT_HAVE_THE_KEY: ["You need the key."],
    YOU_LOSE: ["*** YOU LOSE ***"],
    YOU_TRY_BUT_FAIL: ["You try, but fail."],
    YUMMY: ["Yummy!"],
    HELP: [
      "Available commands:",
      "go direction (up down east west north south); aliases: u d e w n s",
      "pick X, drop X, wear X, put X in|on|over Y, eat X, drink X",
      "look, inspect X, open X, close X, lock X, unlock X",
      "lantern on|off"
    ]
  };

  //=========================================================
  // References for two-word names
  refs = {
    "little key": "little_key",
    "soldier uniform": "soldier_uniform",
    "gold bars": "gold_bars",
    "silver key": "silver_key",
    "soldier corpse": "soldier_corpse",
    "rusted key": "rusted_key",
    "pack of cards": "pack_of_cards",
    "nail bar": "nail_bar",
    "exit gate": "exit_gate",
    "guard corpse": "guard_corpse",
    "iron key": "iron_key"
  };

  //=========================================================
  // Game state
  state = {
    score: { value: 0, writable: true },
    gameState: { value: "playing", writable: true },
    timeLimit: 100,
    elapsedTime: { value: 0, writable: true },
    jackalAware: { value: 0, writable: true },
  };
  
  // Get ready
  getReady();
}

// Start a new game when we load
window.onload = newgame;

// Globals initialized in newgame()
var
  state, ui, milestones,
  map, things, player,
  words, aliases, refs;

/*
 *  UI UPDATE
 */

function updateNavbar() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | Health: " + Math.round(player.health.value) + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
}

function getReady() {
  updateNavbar();
  appendWords("GAME_START");
  waitForInput();
}

function loseGame() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | *** DEAD *** " + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
  appendWords("YOU_LOSE");
  appendLine("<i>Reload the page (F5) or type anything to play again</i>");
  state.gameState.value = "lost";
}

function winGame() {
  var t = getTitle(player.locationInMap.value);
  ui.scoreboard.innerHTML =
    t +
    " | Score: " + state.score.value + 
    " | *** FREE *** " + 
    " | Time left: " + (state.timeLimit - state.elapsedTime.value);
  appendWords("YOU_WIN!");
  appendLine("<i>Reload the page (F5) or type anything to play again</i>");
  state.gameState.value = "won";
}

// Append last command to game output
function appendInputValue(lines) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p class="inputvalue">>&nbsp;' + ui.input.value.toString() + '</p>';
}

// Append text to game output
function appendText(lines) {
  var a = document.getElementById("message");
  var l = "";
  for (let i = 0; i < lines.length; i++) {
    l = l + lines[i] + "<br/>";
  }
  ui.message.innerHTML =
    a.innerHTML + '<p>' + l + "</p>";
}

// Append a single line of text to game output
function appendLine(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p>' + line + '</p>';
}

// Manage paragraphs
function appendNewPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + '<p>' + line;
}

function appendToPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '<br/>';
}

function appendToPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '<br/>';
}

function appendEndPar(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML = a.innerHTML + line + '</p>';
}

// Append RAW text to game output
function appendRawText(line) {
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + '<p>***' + line+ "***</p>";
}

function appendPlaceTitle(p) {
  var t = '<p class="placetitle">' + getTitle(p) + '</p>';
  var a = document.getElementById("message");
  ui.message.innerHTML =
    a.innerHTML + t;
}

/*
 *  WORDS PROCESSING
 */

function getTitle(s) {
  if (words.hasOwnProperty(s)) {
    if (words[s].hasOwnProperty("title")) {
      return words[s].title;
    }
  }
  return "***" + s + "***";
}

function getWord(w, s) {
  var r = "[" + s + ":" + w + "]";
  if (! words.hasOwnProperty(w)) { return r; }
  if (! words[w].hasOwnProperty(s)) { return r; }
  return words[w][s];
}

function processAliases(s) {
  var p = aliases[s];
  if (p == undefined) { return s; }
  else { return p; }
}

function processRefs(s) {
  Object.entries(refs).map(entry => {
    var key = entry[0];
    var v = entry[1];
    s = s.replace(key, v);
  });
  return s;
}

function capitalizeString(s) {
  return s.replace(/^\w/, (c) => c.toUpperCase());
}

function appendWords(s) {
  var p = words[s];
  if (p == undefined) {
    appendText(["[" + s + "]"]);
  } else {
    appendText(p);
  }
}

/*
 *  IN-GAME WORD PROCESSING
 */

function appendPlaceDescription(p) {
  if (words.hasOwnProperty(p) &&
    words[p].hasOwnProperty("description")
  ) {
    var a = words[p].description;
    appendText(a);
  } else {
    var s = "[[[DESCRIPTION: " + p + "]]]";
    appendText([s]);
  }
}

function appendObjectHere(o) {
  if (words.hasOwnProperty(o) &&
    words[o].hasOwnProperty("here")
  ) {
    appendText([ capitalizeString(words[o].here) ]);
  } else {
    var s = "[HERE: " + o + "]";
    appendText([s]);
  }
}

function appendObjectAccessible(o) {
  if (words.hasOwnProperty(o) &&
    words[o].hasOwnProperty("accessible")
  ) {
    appendText([ words[o].accessible ]);
  } else {
    var s = "[ACCESSIBLE: " + o + "]";
    appendText([s]);
  }
}

function appendInspection(o) {
  if (
    (words.hasOwnProperty(o)) &&
    (words[o].hasOwnProperty("inspection"))
  ) {
    appendText(words[o].inspection);
  } else {
    var s = "[INSPECTION: " + o + "]";
    appendText([s]);
  }
}

function appendAllNearby(p) {
  var f = 1;
  Object.entries(things).map(entry => {
    var key = entry[0];
    var l = things[key].locationInMap.value;
    if (l == p) {
      if (f > 0) {
        appendText(["You see:"]);
        f = 0;
      }
      appendObjectHere(key);
    } else if (
      things[key].hasOwnProperty("accessibleFrom") &&
      things[key].accessibleFrom == p
    ) {
      if (f > 0) {
        appendText(["You see:"]);
        f = 0;
      }
      appendObjectAccessible(key);
    }
  });
} 

function appendAllInside(p) {
  var f = 1;
  Object.entries(things).map(entry => {
    var key = entry[0];
    var l = things[key].locationInMap.value;
    if (l == p) {
      if (f > 0) {
//        appendNewPar(getWord(p, "hasInside"));
        appendLine(getWord(p, "hasInside"));
        f = 0;
      }
      if (things[key].hasOwnProperty("contains")) {
        if (things[key].amount.value == 0) {
//          appendToPar(getWord(key, "here") + " (empty)");
          appendLine(capitalizeString(getWord(key, "here")) + " (empty)");
        } else {
//          appendToPar(getWord(key, "here") + " (containing " + getWord(things[key].contains.value, "raw") + ")");
          appendLine(capitalizeString(getWord(key, "here")) + " (containing " + getWord(things[key].contains.value, "raw") + ")");
        }
      } else {
//        appendToPar(getWord(key, "here"));
        appendLine(capitalizeString(getWord(key, "here")));
      }
    }
  });
} 

/*
 *  IN-GAME FUNCTIONS
 */

function killObject(o) {
  var f = o[0];
  if (player.inventory.rightHand.value == f) {
    player.inventory.rightHand.value = undefined;
  } else if (player.inventory.leftHand.value == f) {
    player.inventory.leftHand.value = undefined;
  }
  delete things[f];
}

function isPitchBlack() {
  var h = player.locationInMap.value;
  if (map.pitchBlackPlaces.includes(h)) {
    if (
      (things.lantern.locationInMap.value == "player") &&
      (things.lantern.isOn.value == 1)
    ) { return false; }
    else if (
      (things.lantern.locationInMap.value == h) &&
      (things.lantern.isOn.value == 1)
    ) { return false; }
    else {
      if (things.lantern.isOn.value == 0) {
        return true;
      }
      var lh = things.lantern.locationInMap.value;
      if (things.hasOwnProperty(lh)) {
        if (things[lh].hasOwnProperty("isOpen")) {
          if (things[lh].isOpen.value == 1) {
            return false;
          }
        }
      }
    }
    return true;
  }
  return false;
}

function haveAvailableSlots() {
  if (player.inventory.rightHand.value == undefined ||
    player.inventory.leftHand.value == undefined
  ) {
    return true
  } else {
    return false
  }
}

function haveSword() {
  return (player.inventory.rightHand.value == "sword") ||
    (player.inventory.leftHand.value == "sword");
}

function takeChances(n, m) {
  return ( (m * Math.random()) > n );
}

function calculateDamage(i) {
  var h = player.health.value;
  if (h <= i) { return h; }
  var p = h - i;
  var d = 6.0 * Math.random();
  if (d >= p) { return h; }
  return d;
}

function runHook(h) {
  switch(h) {
    case "reach_restroom":
      things.dying_soldier.locationInMap.value = "limbo";
      things.soldier_corpse.locationInMap.value = "restroom";
      break;
    case "reach_vault":
      if (state.jackalAware.value == 0) {
        state.jackalAware.value = 1;
        if (isPitchBlack()) {
          if (haveSword()) {
            if (takeChances(player.health.value, 24)) {
              checkMilestone("killedJackalPitchBlack");
            } else {
              gameOver("deadFightingJackalPitchBlack");
            }
          } else {
            gameOver("slainedByJackalPitchBlack");
          }
        } else {
          if (haveSword()) {
            checkMilestone("killedJackal");
          } else {
            checkMilestone("jackalHasAwakened");
          }
        }
      } else {
        if (isPitchBlack()) {
          if (haveSword()) {
            if (takeChances(player.health.value, 32)) {
              checkMilestone("killedJackalPitchBlack");
            } else {
              gameOver("deadFightingJackalPitchBlack");
            }
          } else {
            gameOver("slainedByJackalPitchBlack");
          }
        } else {
          if (haveSword()) {
            checkMilestone("killedJackal");
          } else {
            gameOver("slainedByJackal");
          }
        }
      }
      break;
    case "jackalHasAwakened":
      if (! isPitchBlack()) {
        appendInspection("jackal");
      }
      appendLine("Horrified, you retreat to the cellar.");
      player.locationInMap.value = "cellar";
      milestones.reach_vault.done = 0;
      break;
    case "killedJackalPitchBlack":
      var d = calculateDamage(8);
      affectHealth(-d);
      things.jackal.locationInMap.value = "limbo";
      things.jackal_corpse.locationInMap.value = "vault";
      break;
    case "killedJackal":
      var d = calculateDamage(3);
      affectHealth(-d);
      things.jackal.locationInMap.value = "limbo";
      things.jackal_corpse.locationInMap.value = "vault";
      break;
    case "reach_exit":
      gameOver("exit");
      break;
  }
}

function checkMilestone(m) {
  if (milestones.hasOwnProperty(m)) {
    if (milestones[m].done == 0) {
      state.score.value = state.score.value + milestones[m].score;
      milestones[m].done = 1;
      runHook(m);
      if ((state.gameState.value == "lost") || (state.gameState.value == "won")) { return; }
      if (words.hasOwnProperty(m + "_ok")) {
        appendWords(m + "_ok");
//      } else {
//        appendLine("*** MILESTONE:" + m + " ***");
      }
    }
  }
}

function showScore(s) {
  appendText([
    "Score: " + state.score.value,
    s
  ]);
}

function showWinningScore() {
  var l = (state.timeLimit - state.elapsedTime.value);
  a.push("Bonus for remaining time: " + l);
  var h = Math.round(player.health.value);
  var bh = 0;
  if (h > 11) { a.push("Bonus for good health: " + h*2); bh = h*2; }
  var s = 0;
  for (var t in things) {
    if (things[t].locationInMap.value == "player") {
      s = s + things[t].score;
    }
  }
  if (things.bag.locationInMap.value == "player") {
    for (var t in things) {
      if (things[t].locationInMap.value == "bag") {
        s = s + things[t].score;
      }
    }
  }
  var c = 0;
  if (player.clothes.onBody.value = "soldier_uniform") {
    c = c + 20;
  }
  if (player.clothes.onFoot.value = "boots") {
    c = c + 10;
  }
  var w = 0;
  if (things.sword.locationInMap.value == "player") {
    w = w + 30;
  }
  var a = ["Game score: " + state.score.value];
  if (s > 0) { a.push("Bonus for items stolen: " + s); }
  if (c > 0) { a.push("Bonus for wearing soldier clothes: " + c); }
  if (w > 0) { a.push("Bonus for wielding a sword: " + w); }
  appendText(a);
  var f = state.score.value + l + bh + s + c + w;
  appendLine("<b>Your final score: " + f + "</b>")
}

function gameOver(s) {
  switch(s) {
    case "timeUp":
      appendWords("GAMEOVER:timeUp");
      showScore("You have been slained.");
      break;
    case "dead":
      appendWords("GAMEOVER:dead");
      showScore("You died of starvation.");
      break;
    case "deadFightingJackalPitchBlack":
      appendWords("GAMEOVER:deadFightingJackalPitchBlack");
      showScore("Fighting a hungry beast in the dark wasn't a good idea.");
      break;
    case "slainedByJackalPitchBlack":
      appendWords("GAMEOVER:slainedByJackalPitchBlack");
      showScore("You have been torn to pieces.");
      break;
    case "slainedByJackal":
      appendWords("GAMEOVER:slainedByJackal");
      showScore("The jackal ripped your body apart.");
      break;
    case "exit":
      appendWords("GAMEOVER:win");
      showWinningScore();
      winGame();
      return;
    default:
      appendWords("[[[GAMEOVER:" + s + "]]]");
      showScore(s);
  }
  loseGame();
}

function affectHealth(f) {
  var i = player.health.value;
  var d = i + f;
  player.health.value = d;
  if (d < 0.5) { gameOver("dead"); }
  else if (d < 3 && i < d) { appendWords("feelBetterButStarving"); }
  else if (d < 3) { appendWords("feelStarving"); }
  else if (d < 5 && i < d) { appendWords("feelBetterButIll"); }
  else if (d < 5) { appendWords("feelIll"); }
  else if (d > 4 && i < d) { appendWords("feelBetter"); }
  else if (d < 8  && i > d) { appendWords("feelNotTooGood"); }
  else if (d > 7  && i < d) { appendWords("feelAwesome"); }
}

function checkLantern() {
  if (
    things.lantern.isOn.value == 1 &&
    things.lantern.timeOut.value > 0
  ) {
    things.lantern.timeOut.value = things.lantern.timeOut.value - 1;
    if (things.lantern.locationInMap.value == "player") {
      if (things.lantern.timeOut.value < 1) {
        appendWords("LANTERN_IS_DEAD");
        things.lantern.isOn.value = 0;
        if (isPitchBlack()) { appendWords("PITCH_BLACK"); }
      }
      if (
        things.lantern.timeOut.value > 0 &&
        things.lantern.timeOut.value < 8
      ) {
        appendWords("LANTERN_IS_ABOUT_TO_DIE");
      }
    }
  }
}

function advanceTime(i) {
  if (
    (state.gameState.value != "lost") &&
    (state.gameState.value != "won")
  ) {
    state.elapsedTime.value += i;
    affectHealth(-i*0.28);
    checkLantern();
    if (state.elapsedTime.value >= state.timeLimit) {
      gameOver("timeUp");
    }
  }
}

/*
 *  COMMANDS
 */

//==========================================================
// LOOK
function doLook(a) {
  if (a[0] == "at") {
    a = stripFirst(a);
    if (a[0] == undefined) {
      appendWords("BAD_SYNTAX");
      return;
    }
    else { doLookAt(a); }
  } else if (a[0] == undefined || a[0] == "around") {
    doLookAround();
  } else {
    doLookAt(a);
  }
  advanceTime(1);
}

function doLookAround() {
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK");
  } else {
    var h = player.locationInMap.value;
    appendPlaceTitle(h);
    appendPlaceDescription(h);
    appendAllNearby(h);
  }
}

function doLookAt(a) {
  doInspect(a);
}

//==========================================================
// INSPECT
function doInspect(o) {
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK");
    return;
  }
  if (things.hasOwnProperty(o)) {
    tryInspectThing(o);
  } else if (map.doors.hasOwnProperty(o)) {
    tryInspectDoor(o);
  } else {
    appendWords("NOT_FOUND");
  }
}

function tryInspectThing(o) {
  var h = player.locationInMap.value;
  if (things[o].locationInMap.value == h) {
    if (things[o].hasOwnProperty("contains")) {
      if (things[o].amount.value == 0) {
        appendLine(capitalizeString(getWord(o, "here")) + " (empty)");
      } else {
        appendLine(capitalizeString(getWord(o, "here")) + " (containing " + getWord(things[o].contains.value, "raw") + ")");
      }
    } else {
      appendInspection(o);
      if (
        (! things[o].hasOwnProperty("isOpen")) ||
        (things[o].isOpen.value == 1)
      ) {
        appendAllInside(o);
      }
    }
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    if (things[o].hasOwnProperty("contains")) {
      if (things[o].amount.value == 0) {
        appendLine(capitalizeString(getWord(o, "here")) + " (empty)");
      } else {
        appendLine(capitalizeString(getWord(o, "here")) + " (containing " + getWord(things[o].contains.value, "raw") + ")");
      }
    } else {
      appendInspection(o);
      if (
        (! things[o].hasOwnProperty("isOpen")) ||
        (things[o].isOpen.value == 1)
      ) {
        appendAllInside(o);
      }
    }
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          appendInspection(o);
          appendAllInside(o);
        } else {
          appendWords("NOT_FOUND"); return;
        }
      } else {
        appendWords("NOT_FOUND"); return;
      }
    } else {
      appendWords("NOT_FOUND"); return;
    }
  }
  advanceTime(1);
}

function tryInspectDoor(o) {
  var h = player.locationInMap.value;
  // Check if the door is here
  if (map.doors[o].connects.hasOwnProperty(h)) {
    appendInspection(o);
    var open, locked;
    if (map.doors[o].isOpen.value == 1) { open = "open"; }
    else { open = "closed"; }
    if (map.doors[o].hasOwnProperty("isLocked")) {
      if (map.doors[o].isLocked.value == 1) { locked = "locked"; }
      else { locked = "unlocked"; }
    } else { locked = ""; }
    if (open == "open") {
      appendLine(capitalizeString(getWord(o, "denotative") + " is open."));
    } else if (open == "closed" && locked == "unlocked") {
      appendLine(capitalizeString(getWord(o, "denotative") + " is closed."));
    } else {
      appendLine(capitalizeString(getWord(o, "denotative") + " is locked."));
    }
  } else {
    appendWords("NOT_FOUND"); return;
  }
  advanceTime(1);
}

//==========================================================
// PICK
function performPick(o) {
  var s = undefined;
  if (player.inventory.rightHand.value == undefined) {
    s = "rightHand";
  } else {
    s = "leftHand";
  }
  things[o].locationInMap.value = "player";
  player.inventory[s].value = o;
}

function tryPick(o) {
  if (things[o].hasOwnProperty("canBePicked")) {
    if (things[o].canBePicked == 1) {
      if (haveAvailableSlots()) {
        performPick(o);
        if (words.hasOwnProperty(o) &&
          words[o].hasOwnProperty("raw")
        ) {
          appendText([capitalizeString(words[o].raw) + ": picked."]);
        } else {
          appendText(["[RAW:"+o+"]" + ": picked."]);
        }
        advanceTime(1);
      } else {
        appendWords("NO_AVAILABLE_SLOTS");
      }
    } else {
      appendWords("CANNOT_PICK_THAT");
    }
  } else {
    appendWords("CANNOT_PICK_THAT");
  }
}

function doPick(o) {
  var h = player.locationInMap.value;
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK"); return;
  }
  if (! things.hasOwnProperty(o)) {
    appendWords("NOT_FOUND");
  } else if (things[o].locationInMap.value == h) {
    tryPick(o);
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    tryPick(o);
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          tryPick(o);
        } else {
          appendWords("NOT_FOUND");
        }
      } else {
        appendWords("NOT_FOUND");
      }
    } else {
      appendWords("NOT_FOUND");
    }
  }
}

//==========================================================
// WEAR
function performWear(o, s) {
  things[o].locationInMap.value = "player";
  player.clothes[s].value = o;
}

function tryWear(o) {
  if (things[o].hasOwnProperty("wearAs")) {
    if (things[o].canBePicked == 1) { // Redundant...
      var slot = things[o].wearAs;
      if (player.clothes[slot].value == undefined) {
        performWear(o, slot);
        appendText(["Now wearing: " + getWord(o, "raw")]);
        advanceTime(1);
      } else {
        appendWords("NO_AVAILABLE_SLOTS_FOR_CLOTHES");
      }
    } else {
      appendWords("CANNOT_PICK_THAT");
    }
  } else {
    appendWords("CANNOT_WEAR_THAT");
  }
}

function doWear(o) {
  var h = player.locationInMap.value;
  if (isPitchBlack()) {
    appendWords("PITCH_BLACK"); return;
  }
  if (! things.hasOwnProperty(o)) {
    appendWords("NOT_FOUND");
  } else if (things[o].locationInMap.value == h) {
    tryWear(o);
  } else if (
    things[o].hasOwnProperty("accessibleFrom") &&
    things[o].accessibleFrom == h
  ) {
    tryWear(o);
  } else {
    var p = things[o].locationInMap.value;
    if (things.hasOwnProperty(p) &&
      things[p].hasOwnProperty("locationInMap")) {
      if (things[p].locationInMap.value == h || (
        things[p].hasOwnProperty("accessibleFrom") &&
        things[p].accessibleFrom == h
      )) {
        if (
          (! things[p].hasOwnProperty("isOpen")) ||
          (things[p].isOpen.value == 1)
        ) {
          tryWear(o);
        } else {
          appendWords("NOT_FOUND");
        }
      } else {
        appendWords("NOT_FOUND");
      }
    } else {
      appendWords("NOT_FOUND");
    }
  }
}

//==========================================================
// DROP
function doDrop(o) {
  var h = player.locationInMap.value;
  var k = o[0];
  var s = capitalizeString(getWord(o, "raw")) + ": dropped.";
  if (player.inventory.rightHand.value == k) {
    things[o].locationInMap.value = h;
    player.inventory.rightHand.value = undefined;
    appendText([s]);
  } else if (player.inventory.leftHand.value == k) {
    things[o].locationInMap.value = h;
    player.inventory.leftHand.value = undefined;
    appendText([s]);
  } else if (player.clothes.onBody.value == k) {
    things[o].locationInMap.value = h;
    player.clothes.onBody.value = undefined;
    appendText([s]);
  } else if (player.clothes.onFoot.value == k) {
    things[o].locationInMap.value = h;
    player.clothes.onFoot.value = undefined;
    appendText([s]);
  } else {
    appendWords("CANNOT_DROP"); return;
  }
  advanceTime(1);
}

//==========================================================
// PUT
function doPut(a) {
  var t = a[0];
  var w = a[1];
  var c = a[2];
  var v = ["in", "on", "over"]
  var h = player.locationInMap.value;
  if (t == undefined) { appendWords("PUT_WHAT"); return; }
  if ( w == undefined || (! v.includes(w) )) {
    appendWords("BAD_SYNTAX"); return;
  }
  if (c == undefined) { appendWords("BAD_SYNTAX"); return; }
  if (! things.hasOwnProperty(t)) {
    appendWords("NOT_FOUND"); return;
  }
  if (things[t].locationInMap.value != "player") {
    appendWords("NOT_IN_INVENTORY"); return;
  }
  if (! things.hasOwnProperty(c)) {
    appendWords("NOT_FOUND"); return;
  }
  if (
    (things[c].locationInMap.value != "player") &&
    (things[c].locationInMap.value != h)
  ) {
    appendWords("CONTAINER_NOT_HERE"); return;
  }
  if (! things[c].hasOwnProperty("isContainer")) {
    appendWords("NOT_A_CONTAINER"); return;
  }
  if (things[c].containerType != w) {
    appendWords("PHRASED_INCORRECTLY"); return;
  }
  // All OK, proceeding...
  if (player.inventory.rightHand.value == t) {
    player.inventory.rightHand.value = undefined;
  } else if (player.inventory.leftHand.value == t) {
    player.inventory.leftHand.value = undefined;
  } else if (player.clothes.onBody.value == t) {
    player.clothes.onBody.value = undefined;
  } else if (player.clothes.onFoot.value == t) {
    player.clothes.onFoot.value = undefined;
  }
  things[t].locationInMap.value = c;
  appendWords("DONE");
  advanceTime(1);
}

//==========================================================
// OPEN
function doOpen(d) {
  if (d[0] == undefined) {
    appendWords("OPEN_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];

  // The chest opens differently
  if (c == "chest") {
    var h = player.locationInMap.value;
    if (h == "vault") {
      if (things.chest.isOpen == 1) {
        appendWords("ALREADY_OPEN"); return;
      }
      if (
        (things.nail_bar.locationInMap.value == "player") &&
        (things.hammer.locationInMap.value == "player")
      ) {
        things.chest.isOpen.value = 1;
        things.chest.isLocked.value = 0;
        appendWords("OPENING_CHEST");
        checkMilestone("open_" + c);
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_TRY_BUT_FAIL"); return;
      }
    } else {
      appendWords("NOT_FOUND"); return;
    }
  }

  // Everything but the chest
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isOpen.value == 1) {
      appendWords("ALREADY_OPEN"); return;
    }
    if (
      map.doors[c].hasOwnProperty("isLocked") &&
      map.doors[c].isLocked.value == 1
    ) {
      appendWords("IT_IS_LOCKED"); return;
    }
    map.doors[c].isOpen.value = 1;
    appendWords("OPENING_IT");
    checkMilestone("open_" + c);
    advanceTime(1);
    return;
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isOpen")) {
      if (things[c].isOpen.value == 1) {
        appendWords("ALREADY_OPEN"); return;
      }
      if (
        things[c].hasOwnProperty("isLocked") &&
        things[c].isLocked.value == 1
      ) {
        appendWords("IT_IS_LOCKED"); return;
      }
      things[c].isOpen.value = 1;
      appendWords("OPENING_IT");
      checkMilestone("open_" + c);
      advanceTime(1);
      return;
    } else {
      appendWords("CANNOT_BE_OPENED"); return;
    }
  } else {
    appendWords("OPEN_WHAT"); return;
  }
}

//==========================================================
// CLOSE
function doClose(d) {
  if (d[0] == undefined) {
    appendWords("CLOSE_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isOpen.value == 0) {
      appendWords("ALREADY_CLOSE"); return;
    }
    map.doors[c].isOpen.value = 0;
    appendWords("CLOSING_IT");
    advanceTime(1);
    return;
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isOpen")) {
      if (things[c].isOpen.value == 0) {
        appendWords("ALREADY_CLOSED"); return;
      }
      things[c].isOpen.value = 0;
      appendWords("CLOSING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("CANNOT_BE_CLOSED"); return;
    }
  } else {
    appendWords("CLOSE_WHAT"); return;
  }
}

//==========================================================
//  LOCK
function doLock(d) {
  if (d[0] == undefined) {
    appendWords("LOCK_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isLocked.value == 1) {
      appendWords("ALREADY_LOCKED"); return;
    }
    var l = map.doors[c].hasLock;
    if (
      (things.hasOwnProperty(l)) &&
      (things[l].locationInMap.value == "player")
    ) {
      map.doors[c].isLocked.value = 1;
      appendWords("LOCKING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("NEED_THE_KEY"); return;
    }
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isLocked")) {
      if (things[c].isLocked.value == 1) {
        appendWords("ALREADY_LOCKED"); return;
      }
      var l = things[c].hasLock;
      if (
        (things.hasOwnProperty(l)) &&
        (things[l].locationInMap.value == "player")
      ) {
        things[c].isLocked.value = 1;
        appendWords("LOCKING_IT");
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_DONT_HAVE_THE_KEY"); return;
      }
    } else {
      appendWords("CANNOT_BE_LOCKED"); return;
    }
  } else {
    appendWords("LOCK_WHAT"); return;
  }
}

//==========================================================
// UNLOCK
function doUnlock(d) {
  if (d[0] == undefined) {
    appendWords("UNLOCK_WHAT"); return;
  }
  var here = player.locationInMap.value;
  var c = d[0];
  if (map.doors.hasOwnProperty(d)) {
    // Candidate is a door
    if (! map.doors[c].connects.hasOwnProperty(here)) {
      appendWords("DOOR_ISNT_HERE"); return;
    }
    if (map.doors[c].isLocked.value == 0) {
      appendWords("ALREADY_UNLOCKED"); return;
    }
    var l = map.doors[c].hasLock;
    if (
      (things.hasOwnProperty(l)) &&
      (things[l].locationInMap.value == "player")
    ) {
      map.doors[c].isLocked.value = 0;
      appendWords("UNLOCKING_IT");
      advanceTime(1);
      return;
    } else {
      appendWords("NEED_THE_KEY"); return;
    }
  } else if (things.hasOwnProperty(c)) {
    // Candidate is a thing
    if (things[c].locationInMap.value != here) {
      // Thing is not even here
      appendWords("ITS_NOT_HERE"); return;
    }
    if (things[c].hasOwnProperty("isLocked")) {
      if (things[c].isLocked.value == 0) {
        appendWords("ALREADY_UNLOCKED"); return;
      }
      var l = things[c].hasLock;
      if (
        (things.hasOwnProperty(l)) &&
        (things[l].locationInMap.value == "player")
      ) {
        things[c].isLocked.value = 0;
        appendWords("UNLOCKING_IT");
        advanceTime(1);
        return;
      } else {
        appendWords("YOU_DONT_HAVE_THE_KEY"); return;
      }
    } else {
      appendWords("CANNOT_BE_UNLOCKED"); return;
    }
  } else {
    appendWords("UNLOCK_WHAT"); return;
  }
}

//==========================================================
// DISPLACE 
function doDisplace(a) {
  if (a[0] == undefined) {
    appendWords("GO_WHERE");
    return;
  }
  var here = player.locationInMap.value;
  var dir = a[0];
  var dest = map.pathways[here][dir];
  if (dest == undefined) {
    // Check if the direction is valid
    appendWords("CANNOT_GO_THERE");
  } else {
    // Check if there is a door
    for (var door in map.doors) {
      if (
        map.doors[door]["connects"].hasOwnProperty(here)
      &&
        map.doors[door]["connects"][here] == dir
      &&
        map.doors[door]["isOpen"]["value"] == 0
      ) {
        var s = getWord(door, 'denotative');
        s = capitalizeString(s);
        appendText([s + " is closed."]);
        return;
      }
    }
    //player["locationInMap"] = dest;
    player["locationInMap"]["value"] = dest;
    doLookAround();
    advanceTime(1);
    checkMilestone("reach_" + dest);
  }
}

//==========================================================
// EAT
function doEat(f) {
  if (f == undefined) {
    appendWords("DRINK_WHAT"); return;
  }
  if (! things.hasOwnProperty(f)) {
    appendWords("NOT_FOUND"); return;
  }
  if (things[f].locationInMap.value != "player") {
    appendWords("NOT_IN_INVENTORY"); return;
  }
  if (! things[f].hasOwnProperty("isEdible")) {
    appendWords("NOT_EDIBLE"); return;
  }
  // Eating it
  appendWords("YUMMY");
  affectHealth(things[f].nutritionalValue);
  things[f].amount.value = things[f].amount.value - 1;
  if (things[f].amount.value == 0) {
    killObject(f);
    appendLine("(" + capitalizeString(getWord(f, "consumed")) + ")");
  }
  advanceTime(1);
}

//==========================================================
// DRINK
function doDrink(a) {
  var l = a[0];
  if (l == undefined) {
    appendWords("DRINK_WHAT"); return;
  }
  var c;
  var u;
  c = player.inventory.rightHand.value;
  if (
    (c != undefined) &&
    (things[c].hasOwnProperty("contains")) &&
    (things[c].contains.value == l) &&
    (things[c].amount.value > 0)
  ) { u = c; }
  if (u == undefined) {
    c = player.inventory.leftHand.value;
    if (
      (c != undefined) &&
      (things[c].hasOwnProperty("contains")) &&
      (things[c].contains.value == l) &&
      (things[c].amount.value > 0)
    ) { u = c; }
  }
  if (u == undefined) {
    appendWords("NOT_FOUND"); return;
  }
  // Drinking it
  appendWords("SLURP");
  affectHealth(2);
  things[u].amount.value = things[u].amount.value - 1;
  if (things[u].amount.value == 0) {
    appendText([getWord(u, "empty")]);
  }
  advanceTime(1);
}

//==========================================================
// INVENTORY
function doInventory() {
  var f = 0;
  if (player.inventory.rightHand.value != undefined) {
    if (f == 0) {
      f = 1;
      appendText(["You carry:"]);
    }
    appendText([capitalizeString(getWord(player.inventory.rightHand.value, "here")) + " (right hand)"]);
  }
  if (player.inventory.leftHand.value != undefined) {
    if (f == 0) {
      f = 1;
      appendText(["You carry:"]);
    }
    appendText([capitalizeString(getWord(player.inventory.leftHand.value, "here")) + " (left hand)"]);
  }
  var w = 0;
  if (player.clothes.onBody.value != undefined) {
    if (w == 0) {
      w = 1;
      appendText(["You wear:"]);
    }
    appendText([capitalizeString(getWord(player.clothes.onBody.value, "here")) + " (on body)"]);
  }
  if (player.clothes.onFoot.value != undefined) {
    if (w == 0) {
      w = 1;
      appendText(["You wear:"]);
    }
    appendText([capitalizeString(getWord(player.clothes.onFoot.value, "here")) + " (on foot)"]);
  }
  if (w == 0) {
    appendText(["You are naked."]);
  }
}

//==========================================================
// LANTERN
function doLantern(a) {
  var s = a[0];
  if (things.lantern.locationInMap.value != "player") {
    appendWords("NOT_FOUND"); return;
  }
  var dark = isPitchBlack();
  switch(s) {
    case "on":
      if (things.lantern.timeOut < 1) {
        appendWords("LANTERN_ALREADY_DEAD");
      } else if (things.lantern.isOn.value == 1) {
        appendWords("LANTERN_ALREADY_ON");
      } else {
        things.lantern.isOn.value = 1;
        appendWords("LANTERN_IS_NOW_ON");
        if (dark) { doLookAround(); }
        else { advanceTime(1) };
      }
      break;
    case "off":
      if (things.lantern.timeOut < 1) {
        appendWords("LANTERN_ALREADY_DEAD");
      } else if (things.lantern.isOn.value == 0) {
        appendWords("LANTERN_ALREADY_OFF");
      } else {
        things.lantern.isOn.value = 0;
        appendWords("LANTERN_IS_NOW_OFF");
        if (isPitchBlack()) { doLookAround(); }
      }
      break;
  }
}

//==========================================================
// HELP
function doHelp(a) {
  appendWords("HELP");
}

/*
 *  GAME ENGINE
 */

// Compute guess
function handleInput() {
  if ((state.gameState.value == "lost") || (state.gameState.value == "won")) {
    window.location.reload(true);
  }
  // Show command in game output
  appendInputValue();

  // Get the user's guess from the input field
  var gp = this.value.toString().toLowerCase();
  var gpp = processRefs(gp);
  var g = processAliases(gpp);
  var a = g.split(" ");

  switch(a[0]) {
    case "look":
      doLook(stripFirst(a));
      break;
    case "inspect":
      doInspect(stripFirst(a));
      break;
    case "go":
      doDisplace(stripFirst(a));
      break;
    case "pick":
      doPick(stripFirst(a));
      break;
    case "drop":
      doDrop(stripFirst(a));
      break;
    case "put":
      doPut(stripFirst(a));
      break;
    case "wear":
      doWear(stripFirst(a));
      break;
    case "open":
      doOpen(stripFirst(a));
      break;
    case "close":
      doClose(stripFirst(a));
      break;
    case "lock":
      doLock(stripFirst(a));
      break;
    case "unlock":
      doUnlock(stripFirst(a));
      break;
    case "eat":
      doEat(stripFirst(a));
      break;
    case "drink":
      doDrink(stripFirst(a));
      break;
    case "help": //
      doHelp();
      break;
    case "inventory":
      doInventory(stripFirst(a));
      break;
    case "lantern":
      doLantern(stripFirst(a));
      break;
    default:
      appendWords("COMMAND_NOT_FOUND");
      break;
  }
  
  updateNavbar();
  waitForInput();
}

