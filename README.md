# NanoFiction

Some simple games, programmed in javascript, for fun and learning purposes.

- [Home Page](https://spiridon.gitlab.io/nanofiction/index.html)
- [Vault](https://spiridon.gitlab.io/nanofiction/nan0.html) - short Interactive Fiction adventure
- [Bagels](https://spiridon.gitlab.io/nanofiction/bagels2.html) - the classic guessing game
